To get GameInfo

- We need to fetch product lists that containing product IDs
  -  https://core4.omegasys.eu/ics/platform/all?isEnabled=true&noSystemReservedProducts=true&sessionKey=D8FU5J5RIBHJQ671U2J2B8NR15R1DLEI&uType=staff
- Call game info page API with page  https://core4.omegasys.eu/ics/games/page?productIds=234&page=0&size=20000&sort=id%2Cdesc&sessionKey=D8FU5J5RIBHJQ671U2J2B8NR15R1DLEI&uType=staff
  - This data doesn't look like JSON??



-- if we provide 

[7:46 AM] Patrick Mann

Paddy Mann is inviting you to a scheduled Zoom meeting.

Topic: Replication Discussion  Time: Feb 22, 2021 04:45 PM Europe/Malta

Join Zoom Meeting https://zoom.us/j/94477712697?pwd=aG13NUVZMzUyUEJpMDNVaUFFU1Aydz09

Meeting ID: 944 7771 2697 Passcode: 157255 One tap mobile +13462487799,,94477712697#,,,,*157255# US (Houston) +14086380968,,94477712697#,,,,*157255# US (San Jose)

Dial by your location     +1 346 248 7799 US (Houston)     +1 408 638 0968 US (San Jose)     +1 646 876 9923 US (New York)     +1 669 900 6833 US (San Jose)     +1 253 215 8782 US (Tacoma)     +1 301 715 8592 US (Washington DC)     +1 312 626 6799 US (Chicago) Meeting ID: 944 7771 2697 Passcode: 157255 Find your local number: https://zoom.us/u/ac2W7HUhml