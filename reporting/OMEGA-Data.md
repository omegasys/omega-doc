#### RDS Built-in Read ONLY Replica

- https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/SQLServer.ReadReplicas.html **Real Time** read only replica, REQUIRES **ENTERPRISE EDITION!!!**



RDS Database

**1. Create a view**

- all the relevant tables that they need
- base table are not brand specific 

2. **Application**

- Issue the incremental backup command peridically to RDS DB 
- get the backup file location (S3 file URL)
- Connect to the target RDS database, issue log apply / restore command to the target RDS database
- Repeat previous steps





#### Option Jan15: Brand Level Filtered View 

- Regulator need to access SQL Server using SSMS, login as read only db user, direct db access
- OMEGA presents the required tables filtered by brand in the filtered view
- Grant the read only access to the filtered view to regulator 

##### 

#### **OMEGA's Log Shipping** - How it works

- To support AWS, a standard along application called OMEGA-Data is required, that has access to both database instances to manage backup and restore
- Database periodical backup on Primary - every 15 minutes
- Source database uploads backed up files to shared folder or use FTP Upload 
- Target database apply the restored transaction log file from shared folder or FTP Download - every 1 hour
  - During the restore it will have 5 - 10 second freeze (stop the world operation)
- Need machanism clean up, can be done in primary / secondary
- **NOTE**: Brand Level Log Shipping (No Longer works due to Linux Support)
  - How it works before
    - Copy Data to a brand specific database
    - Backup brand specific db
    - Log shipping etc
  - Why does it not work any more?
    - CLR has been deprecated due to compatiblity issue in Linux. Option has been removed

##### Dedicated SQL Server Option (None AWS)

- Existing brand level data log shipping that we built for LSL no longer work, due to CLR library not supportable in Linux env
- Can use Microsoft build-in log shipping framework 
- No need to build ftp folders, self managed
- Can have alert and jobs
- Need to setup by DBA at the begining 



##### Future: Sharding Solution (1.5 Man-Month) 

- Sharding Table
- Routing Table (1 application access different shard)
- Configuration (distributed partition view) table to maint the nodes and shard

Default **sharding** can use <u>BrandID</u> as SharingID. Some tables can be sharded, as it has brand in context. Some of the tables are brand nentual, which is a global table that needs to be replicated as always.

Sharding doesn't always need to use BrandID, we can also create a Tag table, replication can be done via Tag on each table. That means we need to add the tag column to each table. Rows with specific tag of interest can be replicated over.

The solution allow horizontal scaling when a brand need to be split into multiple system. 

**Example**

DB-C1 - Malta (Child Node), DB-C2 - Lisbon (Child Node) DB1- Vancouver (Primary Controller)

- Data Replication from Child Node to Primary Controller 
- Shade CDC, ship to Primary Node, and then batch apply
- Primary node doesn't accept real time insert
- Potentially mjaor system table changes

##### Dependency

- Job System (Standalone application)









-----

Controlled Database

Ability to pull out all the data 

- extracted & aggregated
- 











