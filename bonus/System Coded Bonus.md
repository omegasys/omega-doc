# System Coded Bonus

## Background Overview

At the moment you can also use this box to redeem Coded Bonus and other bonus types with the Bonus Code name. Client has many Coded bonus for the loyalty shop and tournament prizes so if a player finds out the code of an loyalty item (for example 100 euro bonus) or a tournament prize he can fill in the code as many times as he wants and he will get the 100 euro bonus or any other amount depending of the code every time without his points are going down.

## Requirements

Client requested to restrict /ips/redeemBonus to ONLY Coupon Code.

## Analysis

### How /ips/redeemBonus works?

IPS redeemBonus checks the following

- Check Coupon Validity from Bonus_Coupon (1 time used), return used error if coupon is identified and already been used
- If coupon not found, check Bonus_Plan's BonusCode
- If a bonus plan's bonus code matches with the code entered, the following validation would be performed
  - BonusPlanStatus == ACTIVE
  - Today is valid for Bonus Plan Date Range
- If a bonus plan is a recurring plan, check recurring condition
  - Continous
  - Day
  - Time
  - Week
- Check Bonus Plan Fit User
  - User Status Validation
    - Lock Status == NOT_LOCKED
    - Active Status == ACTIVE
    - KYC Status != FAILED
  - Brand Checking
  - Currency Checking
  - Bonus Plan Country Checking (none exists or entry exists both means no restriction for the player's country)
  - Bonus Plan Affilicate Checking
    - If bonus plan is affiliate specific, check affilicate code match user tracking code
  - Bonus Plan User Participation Rule Checking
    - Bonus Plan Included User Checking
    - Bonus Plan Included Tag Checking
    - Bonus Plan Agent Checking
    - Bonus Plan VIP Status Checking
  - Bonus Plan Excluded Tag Checking
  - Bonus Plan Exclusion Checking
    - Check bonuses that had been awarded to user, whose bonus plan in the bonus plan exclusion list
  - Check Max Trigger 
    - Max Trigger All 
    - Max Trigger Player
    - Max Trigger per recurring period
  - Check First Deposit

### Change Proposal

#### DB Update

- Add a new attribute to bonus plan table, called isSstem
  - If a bonus plan is a system bonus plan, that means bonus plan can not be triggered by PLAYER
  - A system bonus plan can ONLY be triggered by STAFF **Manually** or SYSTEM **Automatically** 

#### Bonus Service Update

- When IPS API redeemBonus is been called
  - **Before** Bonus Service checks the validity of the bonus code
    - Return **INVALID_OPERATION** Error if a bonusPlan's **isSystem** **==** **true**

#### CORE Update

##### CORE UI

- Add a new **Switch** to Bonus Plan Create | Edit | View Page 

##### CORE ICS API

- Bonus Plan Create (POST /bonusplan) 
- Bonus Plan Update (PUT /bonusplan) 
- Bonus Plan Get (GET /bonusplan/{id})
- Others?

