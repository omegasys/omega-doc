# External Bonus

## Background 

Many Third party products (3PP) offer bonus capabilities,  some of these bonuses can be generically implemented in OMEGA CORE. However some offer combinations and gaming sequences unique to their system. CORE provides an ability for the operator to do cross-platform promotions, meaning that we can centralize bonus definition, management and reporting for multiple products. However if an operator wishes to be very granular and incentivize players based on unique product attributes  then it is necessary to use the specific promotional features of that product. Sportsbetting is an example where specific combinations, winnings and sequences can trigger specific bonuses. 

## Assumptions

- 3PP has a bonus API interface to trigger bonus for given player(s)

- 3PP has a Wallet / Webservice web hook API to notify OMEGA in order to Cancel/Release/Inform Bonus 

## How to trigger external bonus

- **Setup**

  - Bonus plan with *isPlayable* = **false**, *Additional Award* =  **External Bonus**
    <img src="https://raw.githubusercontent.com/chenzhengbc/md-images/master/uPic/AdditionalAward-ExternalBonus.png" alt="AdditionalAward-ExternalBonus" style="zoom: 50%;" />

    - External Bonus Plan ID from 3PP

    - or External Bonus Code from 3PP

      

- **External Deposit Bonus Plan Workflow**

  1. Player enters the deposit amount
  2. IPS API getDepositEligibleBonusPlan is called
  3. Eligible Deposit Bonus Plan will be returned, including external deposit bonus plans
  4. Then the player select an external deposit bonus plan and click on "next"
  5. Then the IPS processDeposit API would be called, with bonusPlanId provided.
  6. IPS detects that it is external bonus plan, it will invoke 3rd party provider's bonus creation API with externalBonusPlanId configured in BonusPlan as we configured this external bonus plan as DEPOSIT bonus 
  7. Bonus is been issued by 3rd party
  8. Amount is been deducted and Bonus is then been created in PENDING status in OMEGA system

- **Coded Bonus Plan workflow**

  1. Player enter the OMEGA bonus code in the apply bonus code page
  2. Then the IPS applyCodedBonus API would be called
  3. IPS will invoke BIA CreateBonusByCode with externalBonusPlanId configured in BonusPlan as we configured this external bonus plan as CODED bonus. Note the mapped external bonus code will be used to make this API call
  4. Bonus is been issued by 3PP
  5. Bonus is then been created in **PENDING** status in OMEGA system

- **CORE -> Player -> Apply Bonus -> External Bonus**
  <img src="https://raw.githubusercontent.com/chenzhengbc/md-images/master/uPic/PlayerDashboard-ApplyBonus-ExternalBonus.png" alt="PlayerDashboard-ApplyBonus-ExternalBonus" style="zoom: 50%;" />

## How to release external bonus

When 3PP calls OMEGA Wallet End Point to release bonus, OMEGA would

1. Check if external bonus plan with the plan ID exits, create one if it doesn't
2. Check if the player has the external bonus mentioned, create external bonus on the fly if it is not found
3. Release the bonus

<img src="https://raw.githubusercontent.com/chenzhengbc/md-images/master/uPic/ReleaseExternalBonusWalletWorkflow.png" alt="AdditionalAward-ExternalBonus" style="zoom: 50%;" />


#### Cancel Third Party Bonus Plans

- On Withdrawal (optional as in Core today)  
- On Player (IPS) Request 

#### Track and Account for third Party Bonus Plans 

- Bonus Status Created/Expire Amount Wagering Required/Remaining  etc. 