### 3. Architecture and Infrastructure 

#### Short Term - R / W Separation 



![image-20220815221425127](/Users/zhengchen/Dev/omega-doc/kambi/:Users:zhengchen:Library:Application Support:typora-user-images:image-20220815221425127.png)



#### Mid-Term - Microservce based on Event Driven Architecture with Kafka

![image-20220815222539227](/Users/zhengchen/Dev/omega-doc/kambi/:Users:zhengchen:Library:Application Support:typora-user-images:image-20220815222539227.png)

#### Long Term - Sharding

![image-20220815221909770](/Users/zhengchen/Dev/omega-doc/kambi/:Users:zhengchen:Library:Application Support:typora-user-images:image-20220815221909770.png)

### 4. Performance

#### Lab Environment 

##### **Server Spec**

- SQL Server 128GB RAM, 20 Cores
- 2 Txs Servers
- Simulation of wallet transaction only

##### **Throughput**

- 86M transactions per day.
- 1000 transactions per second
- The transactions includes GAME_BET and GAME_WIN transactions

#### Production Environment

##### **Server Spec (High)**

- SQL Server 160GB RAM, 32 Cores
- 2 Txs Servers
- 2 CORE Servers
- 2 PS Servers
- 2 Connect Servers

#####  **Throughput**

- 22M transactions per day
- Peak hour 500-600 transactions per second
- The transactions includes GAME_BET, GAME_WIN, DEPOSIT, TRANSFER etc

 

#### Production Environment #2

##### **Server Spec (Medium)**

- SQL Server 128GB RAM, 16 Cores
- 2 Txs Servers
- 2 CORE Servers
- 5 PS Servers

##### **Throughput**

- 7M transactions per day
- Peak hour 155-220 transactions per second
- The transactions includes GAME_BET, GAME_WIN, DEPOSIT, WITHDRAWAL etc



### 5. **Security** 

#### **Player Security**

- Login (with MFA supported)
  - PS server generates Session Key
- Txs Server
  - Validate Session Key
  - signature to validate payload and originator 

#### **Staff Security**

- Login (with MFA supported)
- OAuth2 (Keycloak server supported)