```
select top 100
	u.[PARTYID], 
		u.[USERID], 
		u.[PASSWORD], 
		u.[user_type],  -- 0 is a user
		u.[ACTIVE_FLAG], 
		u.[CURRENCY], 
		u.[TITLE], 
		u.[FIRST_NAME], 
		u.[LAST_NAME], 
		u.[NICKNAME], 
		u.[GENDER], 

		u.[UNIT], 
		u.[FLOOR_NUMBER], 
		u.[BUILDING], 
		u.[ADDRESS], 
		u.[STREET_NUMBER], 
		u.[CITY], 
		u.[PROVINCE], 
		u.[MUNICIPALITY], 
		u.[COUNTRY], 
		u.[POSTAL_CODE], 
		u.[EMAIL], 
		u.[PHONE], 
		u.[MOBILE_PHONE], 
		u.[LANGUAGE], 
	
		u.[BIRTHDATE],  --'yyyy-mm-dd'
		u.[BIRTH_CITY], 
		u.[BIRTH_COUNTRY], 
		u.[NATIONALITY], 
		u.[NATIONAL_REG_NUMBER], 
		u.[PASSPORT_NUMBER], 
		u.[CARD_SERIAL], 


		u.[HINT_QUESTION], 
		u.[ANSWER], 


		u.[BRANDID], 
	
		u.[REG_DATE], 
		u.[REGISTRATION_STATUS], 
		
		u.[LOCKED_STATUS], 
		u.[LOCKED_UNTIL], 
		u.[LOCKED_BY_STAFFID], 
		u.[LOCKED_SINCE], 
		
		u.[REALITY_CHECK_INTERVAL], 
		u.[SESSION_LIMIT_INTERVAL], 
		u.[MADE_DEPOSIT], 
	
		u.[VIP_STATUS], 
		u.[KYC_STATUS], 
		
		a.BALANCE_REAL

from external_mpt.USER_CONF u
inner join admin_all.ACCOUNT a on u.partyID = a.PARTYID
where u.PARTYID in(91429722, 
91429723)
```

