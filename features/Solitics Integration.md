

### Backend Data Structure

-  Datafeed API 

  - Sample: https://demo.omegasys.eu/ps/ssw/datafeed/v2/transactions?brandId=1&brandPassword=OMEGA&lastTransactionId=9734663

 ```json
    {
       "transactions":[
          {
             "id":9734664,
             "partyId":100252831,
             "userId":"fannyplayer9",
             "gameInfoId":null,
             "gameTranId":null,
             "platformCode":null,
             "platformTranId":null,
             "gameId":null,
             "tranType":"WD_TAX",
             "dateTime":"2020-12-23 08:27:27.423",
             "amount":-0.20,
             "currency":"GBP",
             "balance":10.00,
             "rollbackTranId":null,
             "rollbackTranType":null,
             "balanceReal":0,
             "balanceReleasedBonus":0,
             "balancePlayableBonus":0
          }
       ],
       "status":"SUCCESS"
    }
 ```

​    

### DW Data Structures

#### Data Element: Member

Mapping Tables: external_mpt.user_conf

| Solitics Field                                               | OMEGA Table               | OMEGA Field(s)                                             |
| ------------------------------------------------------------ | ------------------------- | ---------------------------------------------------------- |
| Unique Member                                                | external_mpt.User_Conf    | PartyID                                                    |
| First Name                                                   | external_mpt.User_Conf    | First_Name                                                 |
| Last Name                                                    | external_mpt.User_Conf    | Last_Name                                                  |
| Registration Date                                            | external_mpt.User_Conf    | Reg_Date                                                   |
| Last Update Date Indicator                                   | admin_all.User_Action_Log | Date                                                       |
| Event Type - member registration event \ member update event | admin_all.User_Action_Log | ActionType                                                 |
| Brand                                                        | external_mpt.User_Conf    | BrandID                                                    |
| Currency                                                     | external_mpt.User_Conf    | Currency                                                   |
| Email Address                                                | external_mpt.User_Conf    | Email                                                      |
| Phone Number                                                 | external_mpt.User_Conf    | Mobile_Phone \| Phone                                      |
| Member Group Indicator                                       | external_mpt.User_Conf    | Tag                                                        |
| Member Status                                                | external_mpt.User_Conf    | KYC_Status, Locked_Status, Registration_Status, VIP_Status |
| Test Member Indicator                                        | admin_all.user_registry   | where map_key = 'testPlayer' and value = 'true'            |
| Approval Fields (allow sms, allow emails etc)                | external_mpt.User_Conf    | ContactPreference                                          |



#### Data Element: Withdrawal | Deposit

| Solitics Field                 | OMEGA Table        | OMEGA Field(s)             |
| ------------------------------ | ------------------ | -------------------------- |
| Unique Event ID                | rpt.PaymentStaging | PaymentID                  |
| Unique Member ID               | rpt.PaymentStaging | PartyID                    |
| Type                           | rpt.PaymentStaging | PaymentType                |
| Event Amount                   | rpt.PaymentStaging | Amount                     |
| Event Date                     | rpt.PaymentStaging | Date                       |
| Last Updated Indicator         | rpt.PaymentStaging | requestDate \| ProcessDate |
| Status                         | rpt.PaymentStaging | PaymentStatus              |
| Event Amount - original Amount | rpt.PaymentStaging | Amount                     |
| Event Currency                 | rpt.PaymentStaging | Currency                   |
| Others                         |                    | TBD                        |
|                                |                    |                            |



#### Data Element: BET | WIN (Option #2)

| Solitics Field   | OMEGA Table            | OMEGA Field(s)                                            |
| ---------------- | ---------------------- | --------------------------------------------------------- |
| Unique Event ID  | rpt.AccountTranStaging | AccountTranID                                             |
| Unique Member ID | rpt.AccountTranStaging | PartyID                                                   |
| Amount           | rpt.AccountTranStaging | AmountReal \| AmountReleasedBonus \| AmountPlayableBonus  |
| Event Date       | rpt.AccountTranStaging | Date                                                      |
| Event Currency   | rpt.AccountTranStaging | Currency                                                  |
| Game             | rpt.AccountTranStaging | GameID                                                    |
| Provider         | rpt.AccountTranStaging | ProductID -> ProductTable lookup                          |
| Status           | rpt.AccountTranStaging | RollbackTranID -> if not null then it is been rolled back |
|                  |                        |                                                           |
|                  |                        |                                                           |