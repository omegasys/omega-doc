

- Plan can expired or cancel 
- give the ablity to reenable old plans etc
- separateing sports and financial position (in terms of plans)
- 



- Another API to add or update if they are allow to update themselves 





Form exception

- Exchange not avaialble X->Y 
  - Allow the operator to specify that rate 
  - Support alternative rate provider (all or nothing)
  - 2nd option - Gvie them the ability to input the exchange rate (when the rate is not avaialble)
  - 3rd option - populate the field with suggesting the inverted rate (extension of option 2)
    - We record who (staff) decided to use the override !!!!





Default to the player 



take out agent broker player status , if agency is turned off



```java
private void validateGameCurrency(@NotNull final CreateGameSessionAtWallet request,
                                  @NotNull final GameSessionDto gameSessionDto) {
    if (StringUtils.isNotEmpty(request.getGameCurrency()) &&
            StringUtils.isNotEmpty(gameSessionDto.getGameCurrency())) {
        OmegaValidator.validateTrue(request.getGameCurrency().equalsIgnoreCase(gameSessionDto.getGameCurrency()),
                EnumError.GENERIC_ERROR, String.format(
                        "GameCurrency Mismatch: request.gameCurrency=%s while gameSession.gameCurrency=%s where gameSessionId=%s",
                        request.getGameCurrency(), gameSessionDto.getGameCurrency(),
                        gameSessionDto.getGameSessionId()));
    }
}
```



Find default should return None, even if the ccyConversion is enabled, the gameCurrency doesn't need to be passed if we dont want to.

```java
public String findGameCurrency(final GetGameCurrencyRequest request) {
    final boolean isEnabled = this.isCcyConversionEnabledByBrandAndPlatform(request.getBrandId(),
            request.getProductCode());
    if (isEnabled) {
        GameCurrencyConversionType conversionType = libProductJdbcCache.getCurrencyConversionType(
                request.getProductCode());
        log.info("conversionType={}", conversionType);
        switch (conversionType) {
            case FIXED:
                return findProductCurrency(request.getBrandId(), request.getProductCode(),
                        request.getPlayerCurrency(), request.getGameId());
            case DYNAMIC:
                Preconditions.checkState(StringUtil.isNotEmpty(request.getGameCurrency()),
                        "requestCurrency must not be empty.");
                return request.getGameCurrency();
            default:
                throw new IllegalStateException("Conversion is enabled but conversionType=" + conversionType);
        }
```



- When game session is opened, requestCurrency (gameCurrency) is a parameter passed in. The value can be null 

  

