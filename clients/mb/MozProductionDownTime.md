Mozzart Downtime Between April 29th and July 29th

### Downtime

**May 07, 2021** - we have problems - casino balance is not visible - index rebuit. system on db03 (weak server)

**May 17, 2021** -  we have some alerts from Microgaming, check out : Hi Team, we have had an alert of a high number of API timeouts recently are you aware of any issues. players are constantly complaining that their game play is being interrupted in sense that they are being kicked out of the game play

Hi @Paddy. We have complaines from 2 players in the last 10 min that the they are  delogate automatly from the account and after that they dindn't see the balance on cazino. 
Can you check if was a bug or is a issus? 
Thank you in advance.

there are **resource blocking** for **index rebuilt**, john is working with me on this to identify the root cause.the application is running fine, but the index is highly fragmmentation 

**failed over to db5, the stronger node**

The **tuned auto index scheduler job also applied** 

hi Milos, Tom, we have rebuilt the index also for croatia, around 00:36. the duration is 15 seconds. if you have some requests from player/providers around that time, it is us rebuilding the index online

both Serbia and Croatia has auto index rebuild setup to run every hour, on account tran real time tables, this is to ensure keeping ghost record down. that table should have no more than 1k records in general most of the time
we will continue monitor the index rebuild job in the next few days.
However, just FYI that **Croatia Connection Timeout issue is not directly related to index**. We do this for consistency purpose.

**May 18, 2021**

Miloš, 6:54 AM
Hello team
everything is OK so far, but just got an email from **GreenTube**,We have noticed an increase in response times to our requests starting from yesterday. We haven't observed any degradation, but were wondering if there was any issue on your end that may have caused this to begin?

Paddy, 3:08 AM
Yes understood. Do you have screenshot of the connection issues from EGT (a graph with a timeline of drop offs)? I got one from MG and it'll make our lives a lot easier 

**From Pragmatic**

Hello, all 
We are receiving timeouts from your endpoint 
https://txs.mozzartbet.com/omegatron/spr/pragmatic/casino/authenticate.html
please check and fix

Miloš, 12:38 PM **19:00 - 19:02 UTC**

**the reindex job runs at 9:00, lasted 2 mins, at the time,**

**May 22, 2021**

Miloš, 12:06 AM
Good morning team, We had many reports of issues with the casino balance tonight between 2:30 - 4:30am. Can you please check what happened? Thanks

@Miloš, we have updated the ticket. The root cause of this, is at the time when the transaction look up is performed, SQL Server decided to use a poor index for transaction lookup, which caused scan of the entire transaction table for every account insert at the time. 
The current resolution is we have forced the query plan for transaction lookup (it is a different lookup with where clause that what had already been pinged before). 
Since the query has been pinged to SQL Server SQL Store, wrong index for this type of search would be prevented.

In the event of similar things happen, the quick work around is switch SQL Server to another node, or restart SQL Server. (not ideal, only for emergency) 
Future improvement, is we could centralize the query lookup with sql hint as always to perform transaction lookup.

**May 27, 2021**

hi Omega team :) our casino page in serbia just went dow. 

Nemanja, 5:35 AM This is our side

**May 31, 2021**

Tomislav, 2:15 AM
Hello Omega is anyone working on this ticket MB-593 ? 

**Casino Germania is Down** 

Paddy, 2:16 AM
Tomi 

**the storage was full,** John and I are working on it

**June 10th, 2021** - Miloš, 10:32 AM Hello team. Looks we’re offline Can you please check 

appears that account table is locked. @Cedomir, for emergency steps:
we will use cluadmin.msc to stop the service and restart going forward.
Investigation about index / table lock will be performed today and further analysis will be shared.

for sql server mis behaviour, i think it works all the time with restart. but i will work with John and Robbie and identify what casue this mis-behaviour. We have pinged about 10 queries with hint in the system. probably a new one

it is a hibernate transaction lookup, we have now pinged the query. 
We will convert it to sql stored proc lookup, and use forceseek to force lookup plan

**June 14, 2021**

Marina, 12:58 AM
@Paddy @Yankee @Zheng good morning guys. Can someone please check whats going on as we cant play any games on casino? Thanks!

Miloš, 12:59 AM
@Paddy this is the blocker, monday is our busiest day, please find someone to check it ASAP.

Cedomir, 1:20 AM
you can, just **services** are stucked

**restarting txs1 tomcat**

txs1 recovered

**June 16, 2021**

Morning Omega, 
we are having issues with **Core, it is working very slow,** and several times before opening ex: player by day, it is giving us 504 gateway Time-out @Paddy @Yankee Can someone take a look? MB-574 



**June 25, 2021**

Gorana, 12:53 AM
We are having issues with Core, communication with server is bad, and casino balance is not visible, can someone take a look please? @Paddy 

games are not working too, 503 error

Duarte, 1:32 AM
wait Paddy **ps server is out of memory**

restarting ps server

hmmmm 169.50.179.195 seems to be out of memory since last night

Miloš, 1:34 AM
we are experiencing this problem since Gorana alerted you

Milan, 2:11 AM
working


but I constantly have problem with **Opaeration/Player/Player by day**

I need to restart Core min 15-20x to get into this part

**June 20, 2021 3:34 PM** **Database Downgrade took place**
hi @Cedomir, for your information with downsize of sql server CPU and memory, the system appears to work okay. John has monitored it for a couple of days so far, no issues. Which means Standard edition is okay

Duarte, 3:30 AM
so 169.51.46.100 was out of memory **I think there was some network or db issue around 9:47** because 169.50.180.28 was affected around the same time

**July 5th, 2021**

Hello @all @Zheng @Yankee @Paddy  We have a problem with all games, can someone check please

@Marko  **ps were down**, restarted tomcat

that was the error from tomcat, @Cedomir, is it related to thread pool or nio, the **thread # reaches 10568970**

**Related to evoplay, Evoplay Read timed out** (**MB-614**)

you can investigate how that retry works and to implement some **back off mechanism when service is not responding initially or responding with 403, retry time must be increased** 



**July 6th, 2021**

2021-07-06 21:14:49,663 +0200 SEVERE [LOG_mozzart.com.netent.gameplatform.gameplay.internalservice.gameplay.RealGamePlayServiceImpl] (HTTP-209) J=1625
598560703-152518-B4HI1M4RAF07M Could not send Message., **CAUSE: java.net.SocketTimeoutException:** SocketTimeoutException invoking https://txs.mozzartbet
.com/omegatron/cxf/NetEntWalletV3: Read timed out: com.netent.gameplatform.serviceapi.base.exception.GamePlatformException: Could not send Message., C
AUSE: java.net.SocketTimeoutException: SocketTimeoutException invoking https://txs.mozzartbet.com/omegatron/cxf/NetEntWalletV3: Read timed out



I think it is db index related. Working with John (our DBA) on this still - he had VPN access issue due to iP changes, shall be okay to access now.

Cedomir and I did the db failed over, the real root cause is most likely related to db, as the 2nd time when we check it together, there were some resource blocking there, most likely index related.

It may take us a few hours to get to the bottom of it. will keep everyone posted

Summary of Incidence :
The issue is related to database index, like what we seen in the past.
Previously, what we did, is just ping the indexes that directly caused the issue, and fix that problem.

**Action took place:**

1. we check all the relevant possible indexes thats related to account_tran, and ping them in the query store
2. Reviewed the query execution plan, identified that the blocking is caused by index, further more, it is caused by the process that updates the player balance. 
2.a We have also **tuned the update account process by getting rid of duplicate / unnecessary lookup**.
2b. optimized the duplicate checking logic to check records in hot table first, then the cold table
3. lastly, we have put a job in process, if there are blocking taking place, and it last for 5+ mins, the root blocking process gets automatically cleared.

Future actions:
1. Have reviewed with Cedomir also, and will discuss the possibility of having multi-instance database within one sql server. at least standalone instance for the major serbia brand.
2. Migrate SQL Server next week to another instance, with SQL Server upgrade from 2016 to 2019

Marina, 10:11 PM
Thank you for the detailed report @Zheng, Cedomir advised me last night of actions taken as well, and about the action plan for the future. It is eminent that something has to be done, and I am pleased to hear that you have discussed options which would improve current situation. As how it is at the moment is not good for the business. 
I just hope that the actions you have discussed are going to happen in near future, they are much, much needed. If you ask me for deadline on this, I would say yesterday - that urgent :)


I am aware that we might stay stable now for X days, but its not us who is important in this: Players are. And thats the info we should always have in our minds. Each time that casino goes down, balance is not visible, games are not working, we loose their trust. They start migrating and that affects both sides money wise. 

So please let me know if anything is needed even from our end, we re here to assist (I mean from casino team besides Cedomir, Nemanja ans his team)- just to get this ball rolling and have the issue fixed. 

10:39 PM
Hi @Marina @Cedomir, I am thinking if we were to separate brands, if resource is allowed, we can have 1 database server for Serbia, 1 database server for the rest of the brands. Each server has 148GB RAM with 24core CPU, sql server standard.
We then need a java application server to perform replication to each database server, eventually when everything is ready, we can discontinue existing enterprise edition.
Each environment would have its standalone application servers, to make that totally separated. I will get some feedback from Jim and Grant, and provide final recommendations in the morning.
I am thinking about 2 weeks effort once we receive the new servers. (Replication of 10TB data takes time). There is no downtime during the replication process. Once done, we shutdown everything and provide delta change delivery. 

Anyways, just a thought for now. We will regroup in the morning. I will make sure I keep an eye on this group on a daily basis till everything stablized. 



**July 25th, 2021**

Miloš, 4:11 AM
Hello team
We have some problems with Nigerian casino, getting timeouts from connect.Mozzartbet.ng

Can someone please check?


Miloš, 4:23 AM
Created a task

MB-627

**Cedomir, 5:08 AM @Paddy  we isolated the issue services are fine now**

**July 26, 2021**

**Miloš, 11:18 AM Team :( Problems again Serbia** Casino balance is 0 and games are not available 

ps ps server log is not moving 2021-07-26 19:50:16.711 [http-nio-8081-exec-7967940]  <--- **extremely high thread #**

**@Miloš @Cedomir  there are hacking events taking place**

10.175.192.26 - - [26/Jul/2021:01:49:35 +0200] "GET /examples/servlets/servlet/SessionExample;jsessionid=7ACBC5AE00E2A7BF26189A9447168D13?dataname=foo&datavalue=bar HTTP/1.1" 200 1263
10.175.192.26 - - [26/Jul/2021:01:50:53 +0200] "GET /examples/servlets/servlet/SessionExample;jsessionid=E5C39BF56CE245EF8C52ABD2890BF55E?dataname=foo&datavalue=bar HTTP/1.1" 200 1263
10.175.192.26 - - [26/Jul/2021:01:51:16 +0200] "GET /favicon.ico HTTP/1.1" 200 21630
10.175.192.26 - - [26/Jul/2021:01:53:13 +0200] "GET /examples/servlets/servlet/SessionExample;jsessionid=3528A42A4B0757FD3E389A73131AA385?dataname=foo&datavalue=bar HTTP/1.1" 200 1263
10.175.192.26 - - [26/Jul/2021:01:53:48 +0200] "GET /examples/servlets/servlet/SessionExample;jsessionid=2495C26769C3A5DDCB6B25BB719E0168?dataname=foo&datavalue=bar HTTP/1.1" 200 1263
10.175.192.26 - - [26/Jul/2021:01:54:26 +0200] "GET /favicon.ico HTTP/1.1" 200 21630
10.175.192.26 - - [26/Jul/2021:01:54:36 +0200] "GET /favicon.ico HTTP/1.1" 200 21630
10.175.192.26 - - [26/Jul/2021:01:54:42 +0200] "GET /favicon.ico HTTP/1.1" 200 21630
10.175.192.26 - - [26/Jul/2021:01:56:17 +0200] "GET /examples/servlets/servlet/SessionExample;jsessionid=FAD6DED485140FCD31FCC106E44B2CA6?dataname=foo&datavalue=bar HTTP/1.1" 200 1263
10.175.192.26 - - [26/Jul/2021:01:56:49 +0200] "GET /examples/servlets/servlet/SessionExample;jsessionid=D4486D47851CE15C92DD21288EE24A1E?dataname=foo&datavalue=bar HTTP/1.1" 200 1263
10.175.192.26 - - [26/Jul/2021:01:57:54 +0200] "GET /favicon.ico HTTP/1.1" 200 21630
10.175.192.26 - - [26/Jul/2021:01:58:22 +0200] "GET /examples/servlets/servlet/SessionExample;jsessionid=C15069C7DA379EF735C42F344FC2D1F9?dataname=foo&datavalue=bar HTTP/1.1" 200 1263
10.175.192.26 - - [26/Jul/2021:01:59:09 +0200] "GET /examples/servlets/servlet/SessionExample;jsessionid=D630EDE8D272D7B303D6C55F2AD1FF74?dataname=foo&datavalue=bar HTTP/1.1" 200 1263

205.185.118.230 - - [26/Jul/2021:02:15:25 +0200] "GET /manager/html HTTP/1.1" 401 2474
205.185.118.230 - - [26/Jul/2021:02:15:26 +0200] "GET /manager/html HTTP/1.1" 401 2474
205.185.118.230 - - [26/Jul/2021:02:15:26 +0200] "GET /manager/html HTTP/1.1" 401 2474
205.185.118.230 - - [26/Jul/2021:02:15:26 +0200] "GET /manager/html HTTP/1.1" 401 2474
205.185.118.230 - - [26/Jul/2021:02:15:26 +0200] "GET /manager/html HTTP/1.1" 401 2474
205.185.118.230 - - [26/Jul/2021:02:15:27 +0200] "GET /manager/html HTTP/1.1" 401 2474
205.185.118.230 - - [26/Jul/2021:02:15:27 +0200] "GET /manager/html HTTP/1.1" 401 2474
183.136.225.14 - - [26/Jul/2021:02:56:08 +0200] "GET / HTTP/1.1" 404 994
183.136.225.14 - - [26/Jul/2021:02:56:09 +0200] "GET / HTTP/1.1" 404 994
183.136.225.14 - - [26/Jul/2021:02:56:09 +0200] "GET /favicon.ico HTTP/1.1" 200 21630
10.175.192.2 - - [26/Jul/2021:03:48:31 +0200] "GET / HTTP/1.1" 404 994
10.175.192.2 - - [26/Jul/2021:03:53:06 +0200] "SSTP_DUPLEX_POST /sra_{BA195980-CD49-458b-9E23-C84EE0ADCD75}/ HTTP/1.1" 500 -
10.175.192.2 - - [26/Jul/2021:04:08:53 +0200] "GET / HTTP/1.1" 404 994
45.146.164.110 - - [26/Jul/2021:04:34:01 +0200] "GET / HTTP/1.1" 404 994
45.146.164.110 - - [26/Jul/2021:04:34:01 +0200] "GET /jars HTTP/1.1" 404 1002
10.175.192.2 - - [26/Jul/2021:13:47:42 +0200] "GET /index.php?s=/Index/\think\app/invokefunction&function=call_user_func_array&vars[0]=md5&vars[1][]=HelloThinkPHP21 HTTP/1.1" 404 1012
10.175.192.2 - - [26/Jul/2021:13:47:48 +0200] "GET / HTTP/1.1" 404 994
10.175.192.2 - - [26/Jul/2021:13:47:50 +0200] "GET /wp-content/plugins/wp-file-manager/readme.txt HTTP/1.1" 404 1084
10.175.192.2 - - [26/Jul/2021:13:47:52 +0200] "GET /console/ HTTP/1.1" 404 1010
10.175.192.2 - - [26/Jul/2021:14:20:08 +0200] "GET /remote/fgt_lang?lang=/../../../..//////////dev/cmdb/sslvpn_websession HTTP/1.1" 404 1024
10.175.192.2 - - [26/Jul/2021:17:36:29 +0200] "GET /owa/auth/logon.aspx HTTP/1.1" 404 1032

Cedomir and I have moved the default tomcat ROOT application out of the tomcat, that is the best we can do on tomcat application itself

Cedomir will also review and see what additional service required on the infrastructure to stop such hacking in the meantime. We will be available at all time if there is anything you need from OMEGA

### Reporting Timeout (Not Downtime)

**May 05, 2021** - CORE Revenue Report is timing out 

May 18, 2021 - **MB-574** can someone please take a look at core, revenue by player by day, Doesn’t work for some time

**Game Integration**

**May 06, 2021** - We are experiencing some issues with DEPOSIT OFFER in Malta where we are giving FS as additional prize on deposit, and we are using Pragmatic Play's games: The Dog House and Sweet Bonanza. FS should be added automatically, however we have players complaining they are not receiving FS. I have attached examples in the task ,where we can see message in Omega Core: APPLY FREE SPINS RESPONSE: FAILED
**May 07, 2021** - **Pragmatic games in Nigeria** MB-576



**Reporting Duplicates**

Miloš, 12:36 AM
Hello team
Please give us any answer on mb-623

We must send report in a couple of hours :(


Yankee, 12:41 AM
Hi Miloš Ranđić , the logs from 2021-05-17 is no longer in the servers anymore, hence so we could not find the detailed transaction information from the log. However, from Robbie investigation through the DB there is indeed high numbers of pending sessions in the db during that period. We believed it’s because of the pending session in the DB and causing the system in slow response for some bet/rollback transactions in that period. We have done some tuning in DB recently and the issue should no longer persist.


Gorana, 3:16 AM
Hey, Omega, something is not good. When we open Omega Core to see games, it is showing us double results of games on each country. When we go on date by default then everything is correct. But when we chose 01.07.2021.-23.07.2021. it is showing us twice each game.



It is report by game
Operation/player/game. 


Gorana, 3:30 AM
We made deeper investigation and found out that mistake happened beetwen 5th and 6th July. 


Yankee, 3:32 AM
Hi @Gorana noted, im checking now


Marina, 3:49 AM
@Yankee this is really strange behaviour. Please check this asap as now I am not sure even if our data in Omega Core is correct. Why would there be split on games on Pragmatic and why is it showing more games than we have live on the site? Also is the financial data correct then? This has to be checked urgently 

I checked the PP Back office to make sure we are live with only 1 version of the game (as they have certified versions and non certified) and all is good at that matter. But here in Omega core I see split for games and instead od 159 games if we choose from 1st of July till now I see 319 games in Serbia


Yankee, 3:51 AM

Hey, Omega, something is not good. When we open Omega Core to see games, it is showing us double results of games on each country. When we go on date by default then everything is correct. But when we chose 01.07.2021.-23.07.2021. it is showing us twice each game.
Gorana Vukmirovic, Friday at 3:16 AM
Actually I'm trying to understand the issue the double result of games one each country, maybe can you give me some explanation in details the double result that you mean?

