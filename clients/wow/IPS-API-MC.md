### /ips/login

```
// 20201211070825
// https://demo.omegasys.eu/ps/ips/login?username=omegazheng&password=Omega123&brandId=1

{
  "errors": [
    
  ],
  "status": "SUCCESS",
  "message": null,
  "partyId": "91429636",
  "username": "omegazheng",
  "language": "en",
  "currency": "USD",
  "email": "zheng@omegasys.eu",
  "sessionKey": "XUN1Y8IYRIUOGUJTGVVIG9EVKR06UNHU",
  "parentId": "100106797",
  "level": "3",
  "userType": "0",
  "isFirstLogin": "false",
  "registrationStatus": "PLAYER",
  "pendingLimitConfirmation": "false",
  "country": "CA",
  "kycStatus": "PASS",
  "associateAccounts": [
    {
      "partyId": 91429636,
      "currency": "USD",
      "sessionKey": "XUN1Y8IYRIUOGUJTGVVIG9EVKR06UNHU",
      "isPrimary": true
    },
    {
      "partyId": 100251208,
      "currency": "BTC",
      "sessionKey": "E7CIPSCEI7Y9TJWLUHE4Y9K50UYMRQRQ",
      "isPrimary": false
    }
  ]
```

### /ips/getAssociateAccounts

```
// 20201211071135
// https://demo.omegasys.eu/ps/ips/getAssociateAccounts?sessionKey=XUN1Y8IYRIUOGUJTGVVIG9EVKR06UNHU

{
  "status": "SUCCESS",
  "message": null,
  "sessionKey": "XUN1Y8IYRIUOGUJTGVVIG9EVKR06UNHU",
  "errors": [
    
  ],
  "associateAccounts": [
    {
      "partyId": 91429636,
      "currency": "USD",
      "sessionKey": "XUN1Y8IYRIUOGUJTGVVIG9EVKR06UNHU",
      "isPrimary": true
    },
    {
      "partyId": 100251208,
      "currency": "BTC",
      "sessionKey": "E7CIPSCEI7Y9TJWLUHE4Y9K50UYMRQRQ",
      "isPrimary": false
    },
    {
      "partyId": 100251208,
      "currency": "BTC",
      "sessionKey": "SG7GU3J7GDFMBHCZ4AJVXEUOFDRBTGCB",
      "isPrimary": false
    }
  ]
}
```

### /ips/getBalance

```
// 20201211071333
// https://demo.omegasys.eu/ps/ips/getBalance?sessionKey=XUN1Y8IYRIUOGUJTGVVIG9EVKR06UNHU

{
  "status": "SUCCESS",
  "totalBalance": "2,803,924.68",
  "withdrawableBalance": "2,803,924.68",
  "bonusBalance": "0.00",
  "currency": "USD",
  "loyaltyPoint": "1989685",
  "vipStatus": "BRONZE",
  "message": "",
  "currencyBalances": [
    {
      "totalBalance": "390.74",
      "withdrawableBalance": "390.74",
      "bonusBalance": "0.00",
      "currency": "USD",
      "loyaltyPoint": "1989685"
    },
    {
      "totalBalance": "156.99790000",
      "withdrawableBalance": "156.99790000",
      "bonusBalance": "0.00000000",
      "currency": "BTC",
      "loyaltyPoint": "0"
    }
  ]
}
```

### /ips/getPlayerInfo

```
// 20201211071549
// https://demo.omegasys.eu/ps/ips/getPlayerInfo?sessionKey=XUN1Y8IYRIUOGUJTGVVIG9EVKR06UNHU

{
  "status": "SUCCESS",
  "firstName": "ABC",
  "lastName": "Chen",
  "userId": "omegazheng",
  "nickname": "omegazhenga3a",
  "address": "1234 Southborough Dr.",
  "city": "Vancouver",
  "province": "BC",
  "postalCode": "V6B 2S2",
  "country": "CA",
  "nationality": "AS",
  "language": "en",
  "phone": "99992356",
  "phoneCountryCode": "1",
  "phoneLocalNumber": "99992356",
  "birthDate": "22-03-1983",
  "birthDateFormatted": "1983-03-22 00:00:00",
  "regDate": "2018-04-11 00:00:00",
  "regDateFormatted": "2018-04-11 00:00:00",
  "email": "zheng@omegasys.eu",
  "partyId": "91429636",
  "mobilePhone": "+17788776666",
  "mobileCountryCode": "1",
  "mobileLocalNumber": "7788776666",
  "currency": "USD",
  "currencies": {
    "BTC": {
      "isoCode": "BTC",
      "name": "Bitcoin",
      "numericCode": "999       ",
      "symbol": "BTC                 ",
      "isDefault": false,
      "symbolCode": "BTC",
      "symbolPosition": null,
      "numberDecimalPoint": 8,
      "isVirtual": false,
      "type": "C",
      "refreshInterval": 10,
      "alertThreshold": 2160,
      "stopThreshold": 2880
    },
    "USD": {
      "isoCode": "USD",
      "name": "USD",
      "numericCode": "840       ",
      "symbol": "$                   ",
      "isDefault": false,
      "symbolCode": "$",
      "symbolPosition": null,
      "numberDecimalPoint": 2,
      "isVirtual": false,
      "type": "F",
      "refreshInterval": 1440,
      "alertThreshold": 2160,
      "stopThreshold": 2880
    }
  },
  "vipStatus": "BARON",
  "kycStatus": "PASS",
  "lastLogin": "2020-12-11 07:08:24",
  "lastLoginFormatted": "2020-12-11 07:08:24",
  "level": 3,
  "parentId": 100106797,
  "userType": 0,
  "isAutopay": false,
  "registrationStatus": "PLAYER",
  "sessionKey": "XUN1Y8IYRIUOGUJTGVVIG9EVKR06UNHU",
  "extraInfo": [
    {
      "key": "occupation",
      "value": "Computer Nerd"
    },
    {
      "key": "jing the best",
      "value": "bwin.com someothersite.com"
    },
    {
      "key": "jing the test",
      "value": "bwin.com someothersite.com"
    },
    {
      "key": "CustomerSourceID",
      "value": "1"
    }
  ],
  "gender": "M",
  "contactPreference": "EMAIL",
  "emailVerificationStatus": "UNVERIFIED",
  "docNumber": null,
  "readonlyFields": "",
  "municipality": null,
  "accountNumber": "4444********1111/VISA/11/2022/AUTHORISED/99444433080010801111",
  "idCardNumber": null,
  "socialMedia": {
    "socialLoginPlatform": [
      
    ],
    "hasPwdSetupAfterSocialSignup": null
  }
}
```

### /ips/getTransactionHistoryByCurrency

```
// 20201211071740
// https://demo.omegasys.eu/ps/ips/getTransactionHistoryByCurrency?sessionKey=XUN1Y8IYRIUOGUJTGVVIG9EVKR06UNHU&startDateTime=2020-12-01T12:00:00Z&endDateTime=2020-12-30T12:00:00Z&pageNum=3&pageSize=20&sortingField=postBalance&orderOption=Asc&type=GAME

{
  "status": "SUCCESS",
  "totalRecords": 52,
  "transactionList": [
    {
      "id": 9733672,
      "dateTime": "2020-12-01 12:37:57",
      "dateTimeFormatted": "2020-12-01 12:37:57",
      "tranType": "GAME_BET",
      "platformCode": "PLAYNGO",
      "productCode": "PLAYNGO",
      "segment": "Casino",
      "gameLaunchId": "honeyrush",
      "gameName": "Honey Rush",
      "amount": -2.000000000000000000,
      "amountReal": -2.000000000000000000,
      "amountBonus": 0,
      "postBalance": 1067.120000000000000000,
      "postBalanceReal": 957.120000000000000000,
      "postBalanceBonus": 110.000000000000000000,
      "gameTranId": "1345642",
      "currency": "USD",
      "subProductCode": null
    },
    {
      "id": 9733671,
      "dateTime": "2020-12-01 12:37:53",
      "dateTimeFormatted": "2020-12-01 12:37:53",
      "tranType": "GAME_BET",
      "platformCode": "PLAYNGO",
      "productCode": "PLAYNGO",
      "segment": "Casino",
      "gameLaunchId": "honeyrush",
      "gameName": "Honey Rush",
      "amount": -2.000000000000000000,
      "amountReal": -2.000000000000000000,
      "amountBonus": 0,
      "postBalance": 1069.120000000000000000,
      "postBalanceReal": 959.120000000000000000,
      "postBalanceBonus": 110.000000000000000000,
      "gameTranId": "1345641",
      "currency": "USD",
      "subProductCode": null
    },
    {
      "id": 9733669,
      "dateTime": "2020-12-01 12:37:47",
      "dateTimeFormatted": "2020-12-01 12:37:47",
      "tranType": "GAME_BET",
      "platformCode": "PLAYNGO",
      "productCode": "PLAYNGO",
      "segment": "Casino",
      "gameLaunchId": "honeyrush",
      "gameName": "Honey Rush",
      "amount": -2.000000000000000000,
      "amountReal": -2.000000000000000000,
      "amountBonus": 0,
      "postBalance": 1071.120000000000000000,
      "postBalanceReal": 941.120000000000000000,
      "postBalanceBonus": 130.000000000000000000,
      "gameTranId": "1345640",
      "currency": "USD",
      "subProductCode": null
    },
    {
      "id": 9733668,
      "dateTime": "2020-12-01 12:37:43",
      "dateTimeFormatted": "2020-12-01 12:37:43",
      "tranType": "GAME_BET",
      "platformCode": "PLAYNGO",
      "productCode": "PLAYNGO",
      "segment": "Casino",
      "gameLaunchId": "honeyrush",
      "gameName": "Honey Rush",
      "amount": -2.000000000000000000,
      "amountReal": -2.000000000000000000,
      "amountBonus": 0,
      "postBalance": 1073.120000000000000000,
      "postBalanceReal": 943.120000000000000000,
      "postBalanceBonus": 130.000000000000000000,
      "gameTranId": "1345639",
      "currency": "USD",
      "subProductCode": null
    },
    {
      "id": 9733667,
      "dateTime": "2020-12-01 12:37:31",
      "dateTimeFormatted": "2020-12-01 12:37:31",
      "tranType": "GAME_BET",
      "platformCode": "PLAYNGO",
      "productCode": "PLAYNGO",
      "segment": "Casino",
      "gameLaunchId": "honeyrush",
      "gameName": "Honey Rush",
      "amount": -2.000000000000000000,
      "amountReal": -2.000000000000000000,
      "amountBonus": 0,
      "postBalance": 1075.120000000000000000,
      "postBalanceReal": 945.120000000000000000,
      "postBalanceBonus": 130.000000000000000000,
      "gameTranId": "1345638",
      "currency": "USD",
      "subProductCode": null
    },
    {
      "id": 9733666,
      "dateTime": "2020-12-01 12:37:24",
      "dateTimeFormatted": "2020-12-01 12:37:24",
      "tranType": "GAME_BET",
      "platformCode": "PLAYNGO",
      "productCode": "PLAYNGO",
      "segment": "Casino",
      "gameLaunchId": "honeyrush",
      "gameName": "Honey Rush",
      "amount": -2.000000000000000000,
      "amountReal": -2.000000000000000000,
      "amountBonus": 0,
      "postBalance": 1077.120000000000000000,
      "postBalanceReal": 947.120000000000000000,
      "postBalanceBonus": 130.000000000000000000,
      "gameTranId": "1345637",
      "currency": "USD",
      "subProductCode": null
    },
    {
      "id": 9733665,
      "dateTime": "2020-12-01 12:37:14",
      "dateTimeFormatted": "2020-12-01 12:37:14",
      "tranType": "GAME_BET",
      "platformCode": "PLAYNGO",
      "productCode": "PLAYNGO",
      "segment": "Casino",
      "gameLaunchId": "honeyrush",
      "gameName": "Honey Rush",
      "amount": -2.000000000000000000,
      "amountReal": -2.000000000000000000,
      "amountBonus": 0,
      "postBalance": 1079.120000000000000000,
      "postBalanceReal": 949.120000000000000000,
      "postBalanceBonus": 130.000000000000000000,
      "gameTranId": "1345636",
      "currency": "USD",
      "subProductCode": null
    },
    {
      "id": 9733663,
      "dateTime": "2020-12-01 12:36:18",
      "dateTimeFormatted": "2020-12-01 12:36:18",
      "tranType": "GAME_BET",
      "platformCode": "PLAYNGO",
      "productCode": "PLAYNGO",
      "segment": "Casino",
      "gameLaunchId": "honeyrush",
      "gameName": "Honey Rush",
      "amount": -2.000000000000000000,
      "amountReal": -2.000000000000000000,
      "amountBonus": 0,
      "postBalance": 1080.320000000000000000,
      "postBalanceReal": 950.320000000000000000,
      "postBalanceBonus": 130.000000000000000000,
      "gameTranId": "1345635",
      "currency": "USD",
      "subProductCode": null
    },
    {
      "id": 9733664,
      "dateTime": "2020-12-01 12:36:23",
      "dateTimeFormatted": "2020-12-01 12:36:23",
      "tranType": "GAME_WIN",
      "platformCode": "PLAYNGO",
      "productCode": "PLAYNGO",
      "segment": "Casino",
      "gameLaunchId": "honeyrush",
      "gameName": "Honey Rush",
      "amount": 0.800000000000000000,
      "amountReal": 0.800000000000000000,
      "amountBonus": 0,
      "postBalance": 1081.120000000000000000,
      "postBalanceReal": 951.120000000000000000,
      "postBalanceBonus": 130.000000000000000000,
      "gameTranId": "1345635",
      "currency": "USD",
      "subProductCode": null
    },
    {
      "id": 9733661,
      "dateTime": "2020-12-01 12:35:38",
      "dateTimeFormatted": "2020-12-01 12:35:38",
      "tranType": "GAME_BET",
      "platformCode": "PLAYNGO",
      "productCode": "PLAYNGO",
      "segment": "Casino",
      "gameLaunchId": "honeyrush",
      "gameName": "Honey Rush",
      "amount": -2.000000000000000000,
      "amountReal": -2.000000000000000000,
      "amountBonus": 0,
      "postBalance": 1081.920000000000000000,
      "postBalanceReal": 951.920000000000000000,
      "postBalanceBonus": 130.000000000000000000,
      "gameTranId": "1345634",
      "currency": "USD",
      "subProductCode": null
    },
    {
      "id": 9733662,
      "dateTime": "2020-12-01 12:35:50",
      "dateTimeFormatted": "2020-12-01 12:35:50",
      "tranType": "GAME_WIN",
      "platformCode": "PLAYNGO",
      "productCode": "PLAYNGO",
      "segment": "Casino",
      "gameLaunchId": "honeyrush",
      "gameName": "Honey Rush",
      "amount": 0.400000000000000000,
      "amountReal": 0.400000000000000000,
      "amountBonus": 0,
      "postBalance": 1082.320000000000000000,
      "postBalanceReal": 952.320000000000000000,
      "postBalanceBonus": 130.000000000000000000,
      "gameTranId": "1345634",
      "currency": "USD",
      "subProductCode": null
    },
    {
      "id": 9733660,
      "dateTime": "2020-12-01 12:35:21",
      "dateTimeFormatted": "2020-12-01 12:35:21",
      "tranType": "GAME_BET",
      "platformCode": "PLAYNGO",
      "productCode": "PLAYNGO",
      "segment": "Casino",
      "gameLaunchId": "honeyrush",
      "gameName": "Honey Rush",
      "amount": -2.000000000000000000,
      "amountReal": -2.000000000000000000,
      "amountBonus": 0,
      "postBalance": 1083.920000000000000000,
      "postBalanceReal": 953.920000000000000000,
      "postBalanceBonus": 130.000000000000000000,
      "gameTranId": "1345633",
      "currency": "USD",
      "subProductCode": null
    }
  ]
}
```

### 