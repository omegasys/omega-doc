

![docker-network](omega-docker-network.png)

docker run --name core4 -d -p 8080:8080 -v "/Users/omegazheng/Dev/omegaapps/omega-apps2/OmegaIcs/target/OmegaIcsApps-4.16.0-SNAPSHOT":/usr/local/tomcat/webapps/ics omegasystems/omega-core4 



### Create Network

```
 docker network create -d bridge omega-bridge     
```



### Connect Network

```
docker volume create my-vol
```

### Create Volume ICS 

```
docker create --name ics -v "/Users/omegazheng/Dev/omegaapps/omega-apps2/OmegaIcs/target/OmegaIcsApps-4.16.0-SNAPSHOT":"/usr/local/tomcat/webapps/ics" omegasystems/omega-core4
```

### Create Volume Conf

```
docker create --name conf -v /Users/omegazheng/apps/conf/:/home/omega/apps/conf/ omegasystems/omega-core4
```



### Create Volume Log

```
docker create --name logs -v /Users/omegazheng/logs/:/home/omega/logs/ omegasystems/omega-core4
```

### 

### Create Volume CORE4

```
docker create --name core -v /Users/omegazheng/Dev/omegacore/dist:/usr/local/tomcat/webapps/core omegasystems/omega-core4
```



### Mount the local Folder to Container 

```
docker run --volumes-from ics --volumes-from conf --volumes-from core --volumes-from logs --name core4 --network=omega-bridge -p 8080:8080 -d omegasystems/omega-core4
```

### Connect the network to docker instance

```shell
docker network connect omega-bridge <docker instance name>
```

```
docker network connect omega-bridge mssql
docker network connect omega-bridge core4
```

### Test Network connection

```
docker exec core4 ping mssql
```



### Access Jenkin Artifact

watch out for --auth-no-challenge (order)

```
cd /Users/omegazheng/Dev/omegacore
```

```
RUN wget --user=omegasys --password=11d9b2bb174287311af03fda0f7d671a87 https://jenkins.omegasys.eu/userContent/develop/4.16.0-SNAPSHOT/core.tar.gz --auth-no-challenge && tar -zxf file.tar
```

```
 tar -xjf core.tar.gz  
```

Zheng's token 110b7ea00351e53322c7bd97d13b68552d

```
wget --user=omegasys --password=11d9b2bb174287311af03fda0f7d671a87 https://jenkins.omegasys.eu/userContent/not_relesed/4.12.10/OmegaPs-4.12.10.war --auth-no-challenge
```

```
cd 
```



```
ADD http://foo.com/package.tar.bz2 /tmp/
RUN tar -xjf /tmp/package.tar.bz2 \
  && make -C /tmp/package \
  && rm /tmp/package.tar.bz2
```

```
RUN wget --user=omegasys --password=11d9b2bb174287311af03fda0f7d671a87 https://jenkins.omegasys.eu/userContent/develop/4.16.0-SNAPSHOT/core.tar.gz --auth-no-challenge \
  | tar -xjC /tmp/package \
  && make -C /tmp/package
```



### Debug

Shall use **Remote Debug**

https://blog.jetbrains.com/idea/2019/04/debug-your-java-applications-in-docker-using-intellij-idea/

#### Important

https://www.jetbrains.com/help/idea/debug-a-java-application-using-a-dockerfile.html#create-dockerfile-run-config

- [`9.0.41-jdk8-openjdk-buster`, `9.0-jdk8-openjdk-buster`, `9-jdk8-openjdk-buster`, `jdk8-openjdk-buster`, `9.0.41-jdk8-openjdk`, `9.0-jdk8-openjdk`, `9-jdk8-openjdk`, `jdk8-openjdk`, `9.0.41-jdk8`, `9.0-jdk8`, `9-jdk8`, `jdk8`](https://github.com/docker-library/tomcat/blob/b9416beec5364c00694595d6f3c61b09e764113c/9.0/jdk8/openjdk-buster/Dockerfile)





Docker Compose

- use of .env file to go with docker-compose to inject environment variables
- Values in the shell take precedence over those specified in the `.env` file. If you set `TAG` to a different value in your shell, the substitution in `image` uses that instead:



