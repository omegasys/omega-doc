## OMEGA DB Auto Test

### Auto Test v1

- We have a omega_db_deployment python container, when pull request is made, it will trigger test_install.py against same omega_dev database hosted under 10.0.1.20
- Cons
  - Pre-existing database omega_dev is required
  - contention and false alarm happens often

### Auto Test v2

- Each branch should have its own database image on dockerhub **omegasystems**/**omegadb-dev**  when the pull request is made
- Docker image would be created upon pull request, auto test would be performed against its dedicated docker container, error shall be returned.



### How to build Docker Image **omegasystems**/**omegadb-dev**

Under omegadb project, there is a new image build script `image-dev.sh`, run image-dev.sh would build omegadb-dev from microsoft latest 2019 ubuntu image.

Once the image is built, it can be used as standalone SQL Server docker instance. 

```sh
cd omegadb
sh omegadb-dev-image.sh      
```

