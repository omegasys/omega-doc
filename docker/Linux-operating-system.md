# Linux Operating System

## User Add

### Create user with home directory

Use the `-m` (`--create-home`) option to create the user home directory as `/home/username`:

```sh
sudo useradd -m username
```

The command above creates the new user’s home directory and copies files from `/etc/skel`directory to the user’s home directory. If you [list the files](https://linuxize.com/post/how-to-list-files-in-linux-using-the-ls-command/) in the `/home/username` directory, you will see the initialization files:

```
ls -la /home/username/
drwxr-xr-x 2 username username 4096 Dec 11 11:23 .
drwxr-xr-x 4 root     root     4096 Dec 11 11:23 ..
-rw-r--r-- 1 username username  220 Apr  4  2018 .bash_logout
-rw-r--r-- 1 username username 3771 Apr  4  2018 .bashrc
-rw-r--r-- 1 username username  807 Apr  4  2018 .profile
```

Within the home directory, the user can write, edit and delete files and directories.

### Create user with specified uid

Invoke `useradd` with the `-u` (`--uid`) option to create a user with a specific UID. For example to create a new user named `username` with UID of `1500` you would type:

```shell
sudo useradd -u 1500 username
```

You can verify the user’s UID, using the [`id`](https://linuxize.com/post/id-command-in-linux/) command:

```sh
id -u username
1500
```



### Creating a User with Specific Group ID -g

Linux groups are organization units that are used to organize and administer user accounts in Linux. The primary purpose of groups is to define a set of privileges such as **reading, writing, or executing** permission for a given resource that can be shared among the users within the group.

When creating a new user, the default behavior of the `useradd` command is to **create a group with the same name as the username, and same GID as UID**.

The `-g` (`--gid`) option allows you to create a user with a specific initial login group. You can specify either the group name or the GID number. The group name or GID must already exist.

The following example shows how to create a new user named `username` and set the login group to `users` type:

```sh
sudo useradd -g users username
```

To verify the user’s GID, use the `id` command:

```sh
id -gn username
users
```

### Creating a User and Assign **Multiple Groups** -G

There are two types of groups in Linux operating systems: **Primary group** and **Secondary** (or supplementary) **group**.

Each user can belong to exactly **one primary group** and **zero or more secondary** groups. You to specify a list of supplementary groups which the user will be a member of with the `-G`(`--groups`) option. 

The following command creates a new user named `username` with primary group `users` and secondary groups `wheel` and `docker`. `sudo useradd -g users -G wheel,developers username` 

### `id <username>` to Check User Group

You can check the user groups by typing`id username`
` uid=1002(username) gid=100(users) groups=100(users),10(wheel),993(docker) `

### Creating a User with specific login Shell -s

Creating a User with Specific Login Shell By default, the new user’s login shell is set to the one specified in the `/etc/default/useradd` file. In some distributions the default shell is set to `/bin/sh` while in others it is set to `/bin/bash`.The `-s` (`--shell`) option allows you to specify the new user’s login shell. 

For example, to create a new user named `username` with `/usr/bin/zsh` as a login shell type:`sudo useradd -s /usr/bin/zsh username`
Check the user entry in the [`/etc/passwd`](https://linuxize.com/post/etc-passwd-file/) file to verify the user’s login shell: `grep username /etc/passwd`
`username:x :1001:1001::/home/username:/usr/bin/zsh `

### Creating a User with comments or GECOS -c
Creating a User with Custom Comment The `-c` (`--comment`) option allows you to add a short description for the new user. Typically the user’s full name or the contact information are added as a comment.In the following example, we are creating a new user named `username` with text string `Test User Account` as a comment:`sudo useradd -c "Test User Account" username`The comment is saved in `/etc/passwd` file:
`grep username /etc/passwd`
`username:x :1001:1001:Test User Account:/home/username:/bin/sh `
The comment field is also known as `GECOS`.

### Creating a User with Expiry Date -e `YYYY-MM-DD`
Creating a User with an Expiry Date To define a time at which the new user accounts will expire, use the `-e` (`--expiredate`) option. This is useful for creating temporary accounts.The date must be specified using the `YYYY-MM-DD` format.
For example to create a new user account named `username` with an expiry time set to January 22 2019 you would run:

```sh
sudo useradd -e 2019-01-22 username
```

Use the `chage` command to verify the user account expiry date:
```sh
sudo chage -l username
```
> Last password change : Dec 11, 2018 
Password expires:  never 
Password inactive: never 
Account expires: Jan 22, 2019 
Minimum number of days between password change: 0 
Maximum number of days between password change: 99999 
Number of days of warning before password expires	: 7 
>

Use the `chage` command to verify the user account expiry date:

```sh
sudo chage -l username 
```

The output will look something like this:

> Last password change: Dec 11, 2018 
> Password expires: never 
> Password inactive:  never 
> Account expires: Jan 22, 2019 
> Minimum number of days between password change: 0 
> Maximum number of days between password change: 99999 
> Number of days of warning before password expires: 7 

### Creating a System User with -r

Creating a System User There is no real technical difference between the system and regular (normal) users. Typically system users are created when installing the OS and new packages. Use the `-r` (`--system`) option to create a system user account. 
For example, to create a new system user named `username` you would run:
```sh
sudo useradd -r username
```
System users are created with no expiry date. Their UIDs are chosen from the range of system user IDs specified in the `login.defs` file, which is different than the range used for normal users.

### Changing the Default useradd Values 

The default useradd options can be viewed and changed using the `-D`, `--defaults` option, or by manually editing the values in the `/etc/default/useradd` file. To view the current default options type:

```sh
useradd -D
```
The output will look something like this: 

> GROUP=100 HOME=/home INACTIVE=-1 EXPIRE= SHELL=/bin/sh SKEL=/etc/skel CREATE_MAIL_SPOOL=no

Let’s say you want to change the default login shell from `/bin/sh` to `/bin/bash`. To do that, specify the new shell as shown below:

```sh 
sudo useradd -D -s /bin/bash
```
You can verify that the default shell value is changed by running the following command:

```sh 
sudo useradd -D | grep -i shell
```
> SHELL=/bin/bash



# Reference

## What is in etc/passwd

There are several different authentication schemes that can be used on Linux systems. The most commonly used and standard scheme is to perform authentication against the `/etc/passwd` and [`/etc/shadow`](https://linuxize.com/post/etc-shadow-file/) files.

`/etc/passwd` is a plain text-based database that contains information for all user accounts on the system. It is [owned](https://linuxize.com/post/chmod-command-in-linux/) by root and has 644 [permissions](https://linuxize.com/post/linux-chown-command/) . The file can only be modified by root or users with [sudo](https://linuxize.com/post/sudo-command-in-linux/) privileges and readable by all system users.

Modifying the `/etc/passwd` file by hand should be avoided unless you know what you are doing. Always use a command that is designed for the purpose. For example, to modify a user account, use the [`usermod`](https://linuxize.com/post/usermod-command-in-linux/) command, and to add a new user account use the [`useradd`](https://linuxize.com/post/how-to-create-users-in-linux-using-the-useradd-command/)command.

`/etc/passwd` Format The `/etc/passwd` file is a text file with one entry per line, representing a user account. To view the contents of the file, use a [text editor](https://linuxize.com/post/how-to-use-nano-text-editor/) or a command such as [`cat`](https://linuxize.com/post/linux-cat-command/) :`cat /etc/passwd`Usually, the first line describes the root user, followed by the system and normal user accounts. New entries are appended at the end of the file.Each line of the `/etc/passwd` file contains seven comma-separated 

```
mark:x:1001:1001:mark,,,:/home/mark:/bin/bash
[--] - [--] [--] [-----] [--------] [--------]
|    |   |    |     |         |        |
|    |   |    |     |         |        +-> 7. Login shell
|    |   |    |     |         +----------> 6. Home directory
|    |   |    |     +--------------------> 5. GECOS or Comments
|    |   |    +--------------------------> 4. GID
|    |   +-------------------------------> 3. UID
|    +-----------------------------------> 2. Password
+----------------------------------------> 1. Username
```

1. Username `Username`  The string you type when you log into the system. Each username must be a unique string on the machine. The maximum length of the username is restricted to 32 characters.
2.  `Password`  In older Linux systems, the user’s encrypted password was stored in the `/etc/passwd` file. On most modern systems, this field is set to `x`, and the [user password](https://linuxize.com/post/how-to-change-user-password-in-linux/) is stored in the `/etc/shadow` file.
3.  `UID`  The user identifier is a number assigned to each user. It is used by the operating system to refer to a user.
4.  `GID`  The user’s group identifier number, referring to the user’s primary group. When a user [creates a file](https://linuxize.com/post/create-a-file-in-linux/) , the file’s group is set to this group. Typically, the name of the group is the same as the name of the user. User’s [secondary groups](https://linuxize.com/post/how-to-add-user-to-group-in-linux/) are listed in the `/etc/groups`file.
5.  `GECOS` or the full name of the user. This field contains a list of comma-separated values with the following information: User’s full name or the application name, Room number, Work phone number, Home phone number, Other contact information.
6. `Home directory` The absolute path to the user’s home directory. It contains the user’s files and configurations. By default, the user home directories are named after the name of the user and created under the `/home` directory. 
7. `Login shell`  The absolute path to the user’s login shell. This is the shell that is started when the user logs into the system. On most Linux distributions, the default login shell is Bash.
