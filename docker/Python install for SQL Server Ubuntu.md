### SQL Server 2019 image 

In order to install any applications, please perform `-u root`, as default user mssql doesn't have any permission to do any package update

```sh
docker exec -it -u root mssql bash
```

```
apt update && apt upgrade
```

 apt-get install curl -y

```
curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -

#Ubuntu 18.04
curl https://packages.microsoft.com/config/ubuntu/18.04/prod.list > /etc/apt/sources.list.d/mssql-release.list

apt-get update
ACCEPT_EULA=Y apt-get install msodbcsql17
# optional: for bcp and sqlcmd
ACCEPT_EULA=Y apt-get install mssql-tools
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
source ~/.bashrc
# optional: for unixODBC development headers
apt-get install unixodbc-dev -y
apt install python3-pip -y
```

