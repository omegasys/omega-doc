How to handle db migrations on omegasys_demo

- DB Migration Docker container is avaialable from OMEGA Servers Page https://omegasys.atlassian.net/wiki/spaces/OPERATIONS/pages/1119257048/OMEGA+Servers 10.0.1.29 **dev-docker**

- ssh to Dev Docker instance

- docker run with latest tag from docker.io/omegasystems/omega_db_deployment, typically is 4.x.x-SNAPSHOT

- ```shell
  sudo docker run --rm "docker.io/omegasystems/omega_db_deployment:<tag>" install.py 10.0.1.20,1433 <database> sa <sa password>
  ```

- If the latest SNAPSHOT is out of date or not been avaialble, login to jenkins to generate the docker image **CREATE_DOCKER_IMAGES_NOT_RELEASED** --> This maps to the snapshot version 

  - ![BuildImageFromJenkins](/Users/omegazheng/Dev/omega-doc/docker/JenkinBuildLatestDocker.png)

Confirm that the dockerhub contains the latest images, usally the develop branch would have 4.xx.0-SNAPSHOT-beta

![image-20210210091508287](/Users/omegazheng/Library/Application%20Support/typora-user-images/image-20210210091508287.png)



![image-20210210091955469](/Users/omegazheng/Library/Application%20Support/typora-user-images/image-20210210091955469.png)

Have to execute docker run using **sudo** in this docker host.

```shell
sudo docker run --rm "docker.io/omegasystems/omega_db_deployment:<tag>" install.py <ip> <database_name> sa <sa_password>
```

exmaple below the tag = 4.17.0-SNAPSHOT-beta., database_name = omegasys_demo

```
[omega@dev-docker ~]$ sudo docker run --rm "docker.io/omegasystems/omega_db_deployment:4.17.0-SNAPSHOT-beta" install.py 10.0.1.20,1433 omegasys_demo sa '!0m3ga2016'
```

