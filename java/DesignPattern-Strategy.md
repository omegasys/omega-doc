## Template method pattern

*The template method pattern is a design pattern that allows a group of interchangeable, similarly structured, multi-step algorithms to be defined. Each algorithm follows the **same series of actions** but provides a different implementation of the steps.*

This design pattern allows you to change the behavior of a class at run-time. It is almost the same as Strategy design pattern but with a significant difference. 

- While the Strategy design pattern overrides the entire algorithm
- Template method allows you to change only some parts of the behavior algorithm using abstract operations (the entire algorithm is separated into several operations).



![Image 10](https://www.codeproject.com/KB/architecture/455228/template_method.jpg)