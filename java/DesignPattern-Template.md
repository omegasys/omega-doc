## Strategy pattern

*The strategy pattern is a design pattern that allows a set of similar algorithms to be defined and encapsulated in their own classes. The algorithm to be used for a particular purpose may then be selected at run-time according to your requirements.*

This design pattern can be used in common situations where classes differ only in behavior. In this case it is a good idea to separate these behavior algorithms in separate classes which can be selected at run-time. This pattern defines a family of algorithms, encapsulates each one, and makes them interchangeable. This separation allows behaviors may vary independently of clients that use it. It also increases the flexibility of the application allowing to add new algorithms in future.