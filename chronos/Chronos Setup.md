Chronos Setup

- Need to create Role Chronos
- Need to assgin Chronos Role to the user
  - Note that even with super admin (Full Access), the access of Chronos is still need to be given.
    - i,e. security.PrincialSecurable, the principal should be the role ID of the Chronos been created
    - and The securable should be 3000 (chronos) from db initializtion scirpt. 
- Turn on ICS Authentication from Maria DB, Property Table ***authentication.enabled***
  - Should get rid of it in the future the security.Enable
- In order for the settings to work, ICS need to allow Chronos Application to call 
  - Which means CORS settings need to be allowed from infrastructure as well as the properties.yml file for ICS. 

![image-20220412121901086](image-20220412121901086.png)



start as standalone jar

![image-20220412121922351](image-20220412121922351.png)



maria 10.0.1.120



![image-20220412122206408](image-20220412122206408.png)



.29 is the cluster

![image-20220412122229089](image-20220412122229089.png)