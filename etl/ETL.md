## OMEGA ETL Overview

In order to provide standarnd replications across differnet database platforms inlcuding SQL Server for windows, linux, RDS, Azure, with various editions, previous replication framework doesn't work anymore.

OMEGA ETL (Extract, Transform and Load)  is built to support standard replication across multiple SQL Server database platform.

The use cases for ETL 1.0.0 io to support omega database replication with specific brand(s) filters. Only specified brand data would be replicated to the target databases. The only party that could update target database is OMEGA ETL in 1.0.0, in another words, the target database can be considered as READ ONLY.

## OMEGA ETL Architecture Diagram

![ETL-Architecture-Diagram](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/ETL-Architecture-Diagram.png)

## OMEGA ETL 1.0.0 

![ETL1.0.0RoadMap](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/ETL-V1.0.0-RoadMap.png)



---


## ETL Table Overview

![ETL-Application-Design](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/ETLv1.0.0-Application-Design.png)

### maint.SynchronizationPublisher

**SynchronizationPublisher** defines the <mark>Target Connection String </mark>

```java
jdbc:sqlserver://192.168.2.132:1433;databaseName=Test;user=sa;password=Omega123
```

### maint.SynchronizationPublisherBrand

Defines the Brand Filter for the target database, a.k.a **publisher**. Multiple brands are allowed.

### maint.BrandFilterTableDependencies

- Defines the table depedencies for brand level replication. The default value of the table is maintained by db initialization script `admin_all.usp_DataInitializationMaint`

- When new tables been added, a script called `usp_SynchronizationCreatePublisherTable` shall be called to add the table to the synchronization manually. Otherwise the table will not be picked up by the target by default.

#### view maint.v_SynchronizableTables

- The view maintance the tables that we would like to include in the synchronize process. The excludsion list is predefined in the views
- Synchronizable table must have row version

### maint.SynchronizationConfiguration

- Setup of data source from source tables
- Each source can be as simple as a single table or as complicated as query that joins tables & views.
- Fields
  - Initialization Query
  - Filter query
  - Source
  - Target (Default is a view with instead of trigger)
  - BatchSize

\- Use proc maint.usp_SynchronizationGenerateInitializationScript to populate required steps/columns

\- Use proc maint.usp_SynchronizationGenerateConfiguration to generate required configuration

### maint.SynchronizationProgress

maintains the progress of each synchronization. If the synchronization got interupted, it knows where to pick things up

- FromValue
- ToValue
- FromValueAdjustment
- Last error will be reocrded

### maint.SynchronizationHistory

- Any replicate update > 0 will be recorded

maint.SyncronizationXXX proc 

## Stored Proc Overview

### maint.usp_SynchronizationCreatePublisherTable

- Whenever there are table definition changes, this stored proc needs to be <mark>manually</mark> invoked today <Feb 8, 2021>
- Need to know which confiuration id to replicate

### maint.usp_SynchronizationCreatePublisherTableAll

### Other stored procedures TBD

## Setup Guideline

![ETL-Setup-Guide](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/ETL-Setup-Guide.png)

### **Setup Check List**

- [ ] Access to **OMEGA DockerHub** for ETL Application image
- [ ] An container enviornment that fire up ETL application in container. 
- [ ] Server Spec: 4 CPU, 50 Threads, 8-16GB RAM
- [ ] The ETL application need to have **network**,  **Read Write** (**R+W**) access to both source database and target dataase
- [ ] Target database needs to be created in advance, with connection string ready to be configured in ETL Application

### Assumation on Target Database 

- SQL Instance availale, database has been created
- Username and password / connection string provided
- Will deploy Table and Index ONLY

----

## Q & A

> What shall we do when there are  table definition conflict?

Call `maint.usp_SynchronizationCreatePublisherTable` to reinitilizae the target table

> Any problem with Timezone difference?

TBD

## <mark>Future improvement</mark>

- If synchonization detecture DDL change and synchronization fails, auto re-initialization shall be triggered by the ETL application 