| OMEGA                            | PaymentIQ  | Description |
| -------------------------------- | ---------- | ----------- |
| UserConf.PartyID                 | UserID     |             |
| Payment.Method=PAYMENTIQ_ALL     | -          |             |
| Payment.SubMethod                | Provider   |             |
| -                                | pspService |             |
| Payment.REF_NUMBER               | txRefId    |             |
| Payment.Provider_Tran_ID         | -          |             |
| Payment.Type=DEPOSIT\|WITHDRAWAL |            |             |
| Payment.Status                   |            |             |
| Payment.REQUEST_DATE             |            |             |
| Payment.PROCESS_DATE             |            |             |
| Payment.Amount                   |            |             |
| Payment.Amount_Real              |            |             |
| Payment.Amount_Released_Bonus    |            |             |
| Payment.Req_Bonus_Plan_ID        |            |             |
| Payment.Fee                      |            |             |
| Payment.Last_Updated_Date        |            |             |
| Payment.Request_Amount           |            |             |
| Payment.Request_Method           |            |             |
|                                  |            |             |
|                                  |            |             |
|                                  |            |             |

