### Configuration of PaymentIQ Back Office

  <!--<integrationService>mockMerchantIntegrationService</integrationService>-->
  <integrationService>standardMerchantIntegrationService</integrationService>
  <apiIntegrationUrl>https://cmp-ps.omegasys.eu/ps/ips/paymentiq/{action}</apiIntegrationUrl>





<com.devcode.paymentiq.integration.merchant.MerchantConfig>
  <enabled>true</enabled>
  <!--<integrationService>mockMerchantIntegrationService</integrationService>-->
  <integrationService>standardMerchantIntegrationService</integrationService>
  <apiIntegrationUrl>https://cmp-ps.omegasys.eu/ps/ips/paymentiq/{action}</apiIntegrationUrl>
  <externalUrls/>

  <errorCodeMapping>
    <entries>
      <statusCodeEntry>
        <response>100</response>
        <statusCode>ERR_DECLINED_NO_FUNDS</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>110</response>
        <statusCode>ERR_DECLINED_BAD_REQUEST</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>200</response>
        <statusCode>ERR_DECLINED_LIMIT_OVERDRAWN</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>201</response>
        <statusCode>ERR_DECLINED_LIMIT_OVERDRAWN</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>202</response>
        <statusCode>ERR_DECLINED_LIMIT_OVERDRAWN</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>203</response>
        <statusCode>ERR_DECLINED_LIMIT_OVERDRAWN</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>204</response>
        <statusCode>ERR_DECLINED_LIMIT_OVERDRAWN</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>205</response>
        <statusCode>ERR_DECLINED_LIMIT_OVERDRAWN</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>250</response>
        <statusCode>ERR_DECLINED_LIMIT_OVERDRAWN</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>300</response>
        <statusCode>ERR_NOT_AUTHENTICATED</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>400</response>
        <statusCode>ERR_DECLINED_BAD_REQUEST</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>500</response>
        <statusCode>ERR_MERCHANT_OUT_OF_SERVICE</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>521</response>
        <statusCode>ERR_DECLINED_KYC_BLOCK</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>100500</response>
        <statusCode>ERR_DECLINED_OTHER_REASON</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>100501</response>
        <statusCode>ERR_ABOVE_LIMIT</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>100502</response>
        <statusCode>ERR_DECLINE_USER_NOT_FOUND</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>100503</response>
        <statusCode>ERR_DECLINED_BAD_REQUEST</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>100504</response>
        <statusCode>ERR_DECLINED_NO_FUNDS</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>100510</response>
        <statusCode>ERR_DECLINED_KYC_CHECK_FAILED</statusCode>
      </statusCodeEntry>
      <statusCodeEntry>
        <response>100511</response>
        <statusCode>ERR_RESPONSIBLE_GAMING_LIMIT</statusCode>
      </statusCodeEntry>
    </entries>
  </errorCodeMapping>

![](payment-iq/piq-back-office-merchant-config.png)





![image-20210108164449925](/Users/omegazheng/Library/Application%20Support/typora-user-images/image-20210108164449925.png)





![image-20210108164550419](/Users/omegazheng/Library/Application%20Support/typora-user-images/image-20210108164550419.png)



### PaymentIQ Introduction Video

https://developer-api.paymentiq.io/assets/public/video/PaymentIQ_ShortSummary.mp4