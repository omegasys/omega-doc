## How payments are been used

- Regular PS/IPS Payment Integration, Deposit / Withdrawal
- OMEGA CORE3/4 Cashier Back Office UI 
  - Staff manaully handles the deposit / withdrawal over the counter 
- IPS Cashier 
  - 3rd party payment providers such as PaymentIQ and Praxis take full control of payment life cycle, no back office administration required on OMEGA
- Agency Deposit | Withdrawal | Transfer
- Secondary Wallet Transfers
  - can secondary wallet transfer be considered as Deposit/Withdrawal, while the transaction is actually TRANSFER_IN/OUT?

## Requirements

- IPS API for payment history need to be able to see the payment details

  - if there are bonus been awarded during deposit, it is better to be presented together, i.e., bonusId <-> paymentId

- Payment & Bonus

  - When bonus is cancelled by withdrawal, the payment_id on bonus table also need to be set (I think it is the case today)
  - When bonus is created by deposit, the payment_id or a new column on bonus table need to be set (This is pending)
  - 1 Payment_ID is NOT Sufficident to handle both cases, also FK and Index needs to be reviewed

- Payment reporting in DW

- Escrow

  - When withdrawal is been requested

    - Withdrawal request amount goes to escrow
    - Withdrawal request shouldn't affect player's balance in total
    - IPS API getBalance should return money in Real, Released, Playable and **Escrow (on hold)**
    - CORE Player Dashboard Escrow Fund shall be reflected
- Cash Liability Report need to present Escrow funds in Real, ReleasedBonus (potentially Playable if requried)
  
- **Withdrawal Rollback | Reversal?**

  - Typically, will provide the provider original transaction id, provider's new transaction id, and the amount for reversal / rollback.
  
  - Example of PaymentIQ, it is a cashier payment, will send us payment completion, then a reversal of previous completed payment, with **originalTxsId** and new **txsId**.
  
    ```json
     request={
      "userId": "91477452",
      "txAmount": "10000",
      "txAmountCy": "CLP",
      "txId": "313908575",
      "txTypeId": 954,
      "txName": "Reversal",
      "provider": "AstroPayBank",
      "originTxId": "313163459",
      "accountId": "6c5b08c9-da44-4474-b284-4282685b30b4",
      "maskedAccount": "0495897382050996724C",
      "attributes": {}
    }
    ```
  
    
  
  - **OMEGA Payment Status**
  
    - PENDING
    - COMPLETED
    - FAILED
    - CANCELLED
    - REJECTED
    - BATCHED??
    - BATCH_SENT??
    - RUNNING?
    - SENT_TO_AGENT
    - DP_ROLLBACK
    - WD_ROLLBACK
  
  - Provider Status
  
    - Epcopay- OK, NOTOK, DECLINED, PENDING
  
- PaymentToken - Save this info during **Successful Deposit**

  - ID
  - PartyID
  - PaymentMethod
  - CardType
  - Last4Digit
  - ExpiryDate
  - Token
  - Deleted

  

## Withdrawal History

It is for worldpay only?

## Current Issues 

- Deposit & Withdrawal the requested_amount, requested_method, requested_sub_methods fields are not always been set properly

- Payment Table meta data

  - Misleading term **Conversion** 
    - ConvAmount, ConvRate, ConvCurrency -->  transactAmount, transactCurrency and TransactRate

- Current Payment status doesn't really tell the full story for Deposit &  Withdrawal 

  - Different tables have to be used but not consistent. Such as withdrawal, withdrawal reqeust etc

- Unable to keep track of Escrow Funds easily - Money on hold for players, or operator's liability

  

## Consumers of Payment Data 

- Cash Liablity Report (CLR) needs to know Payment Escrow
  - End of each day, how many payments have been processed, partially processed, pending
  - Partially procesed, still due amount
- System Operational Dashboard needs to display Completed Payments
  - In the event of Withdrawal been splited and paritially processed, only the processed one shall be accounted for 
- Reporting DW
  - Need to have payment aggregation based on fact
- Chronos Events
  - Need to push real time events to external event system

## CurrencyConversionMargin

Is this feature still in use? Can we deprecate this?

## Payment Fee 

OMEGA charged payment fee configuration is currently been defined in OMEGA Payment_Fee table. Payment Fee would be charged AFTER a payment has been successfully completed. With TranType = 'PAYMENT_FEE'.

Payment Table is not affected if there is a fee configured.

## Withdrawal Tax

Withdrawal Tax setting is stored in brand registry | registry hash, with contry configuration. Withdrawal Tax is applied.

1 Payment, 1 Withdrawal AccountTran, 1 WD_TAX AccountTran

## Withdrawal Cancel Bonus

Withdrawal will cancel bonuses that can be cancelled. The attribute from bonus plan cancelOnWithdrawal determines if a bonus shall be cancelled or not upon withdrawal.

Bonus in Active or Queued status would be cancelled

When player requested the withdrawal, bonuses would be be cancelled. 

## Deposit Rollback

- Rollback is depending on admin_all.payment.ReferenceNumber
- Rollback will have original request number and new request number
- if payment is not COMPLETED, it can not be rolled back
- There is a table called **admin_all.Payment_Rollback**, stores the current payment id and the old payment id
- If payment had been rolledback, it can not be rolled back again
- Bonuses thats associated with the original payment shall be cancelled as well



## Payment Use Cases

### Use Case #1 Normal Deposit

#### PreCondition

| PartyID | Real Balance | RB Balance | PB Balance |
| :-----: | :----------: | :--------: | :--------: |
|    1    |      0       |     0      |     0      |

#### Action

VISA deposit $10,  with bonus plan 111 selected

#### Steps

1. IPS API - ips/processDeposit, VISA Deposit request $10, with BonusPlanID=110 selected.  OMEGA sends request to payment provider 

**Account** - N/A

**AccountTran** - N/A

**Payment** been created in PENDING

| PAYMENT_ID | TYPE    | STATUS  | REQ AMT | PROCESSED AMT | METHOD        | PLAN_ID |
| :--------: | :-----: | :-----: | :------------------------: | :-----------: | :---------: | :---------: |
| 1          | DEPOSIT | PENDING | 10 | 0                   | WorldPay_VISA | 110         |

**BONUS** - N/A

2. Payment provider calls OMEGA webhook, completes the payment

**AccountTran** record created

| ACCOUNT_TRAN_ID | TRAN_TYPE | AMOUNT_REAL | AMOUNT_RELEASED_BONUS |
| :-------------: | :-------: | :---------: | :-------------------: |
|       10        |  DEPOSIT  |     +10     |           0           |

**Account** Updated

| PARTYID | BALANCE_REAL | BALANCE_RELEASED_BONUS |
| :-----: | :----------: | :--------------------: |
|    1    |      10      |           0            |

- Payment status **PENDING** -> **COMPLETED**
- Bonus plan gets checked against amount, then bonus been created





### Use Case #2 Withdrawal

PreCondition

**ACCOUNT**

| PARTYID | BALANCE_REAL | BALANCE_RELEASED_BONUS |
| :-----: | :----------: | :--------------------: |
|    1    |      15      |           10           |

VISA withdrawal $21

1. IPS API - ips/processWithdrawal, VISA withdrawal request $21, cancel bonus with BonusPlanID=110, cancel bonus amount $100.   
   - Withdrawal Payment will be created in pending
   - Account will be deudcted 
   - AccountTran will be created
   - Bonus will be cancelled

**ACCOUNT**

| PARTYID | BALANCE_REAL | BALANCE_RELEASED_BONUS |
| :-----: | :----------: | :--------------------: |
|    1    |      0       |           4            |

**ACCOUNT_TRAN**

| ACCOUNT_TRAN_ID | TRAN_TYPE  | AMOUNT_REAL | AMOUNT_RELEASED_BONUS |
| :-------------: | :--------: | :---------: | :-------------------: |
|       10        | WITHDRAWAL |     -15     |          -6           |

**PaymentRequest**

| PAYMENT_ID |    TYPE    | STATUS  | REQ AMT | PROCESSED AMT |    METHOD     | PLAN_ID |
| :--------: | :--------: | :-----: | :-----: | :-----------: | :-----------: | :-----: |
|     10     | WITHDRAWAL | PENDING |   21    |       0       | WorldPay_VISA |         |

**BONUS**

| BONUS_ID | BONUS_PLAN_ID | PARTY_ID | AMOUNT |  PB  |  RB  | STATUS |
| :------: | :-----------: | :------: | :----: | :--: | :--: | :----: |
|          |               |          |        |      |      |        |

2. Payment provider calls OMEGA webhook, completes the payment

- AccountTran record created
- Account updated
- Payment status **PENDING** -> **COMPLETED**
- Bonus plan gets checked against amount, then bonus been created





1. IPS Process Deposit $10
2. Payment $10 Pending
3. Deposit Call back -> Completed | Failed 
   1. BonusPlanID associated with Payment
      1. Triggers the bonus, using the bonusplanID if applicable
      2. When bonus is given, we cant see how much bonus is been given for that deposit (its not been displayed in a single row/transaction)
4. Completed -> Account + $10
5. AccountTran Deposit  $10

Payment Fee  10, fee 1 --> 

AccountTran

we credit the account in the amount we receive!!!

AccountTran Deposit? +10

Fee -1

### Withdrawal 

1. IPS Request Withdrawal  $5000

2. Account -$5000

3. AccountTran Withdrawal -$5000, in the same transaction show +5000 (somewhere)
   1. AccountTranID=55555
   2. AccountTranExtension
   
4. Payment -$5000 Pending

5. Escrow (Player) +$5000

6. Day 1 - Back Office Apporved $2000. -> Payment Pending, Set **RequestedDate**. TODO See below

   1. Process Amount  $-2000 - COMPLETED date, note: **RequestedAmount** should be $-5000,  and Set **ProcessDate **

   2. New new payment record, AMOUNT -3000, **PENDING**, and link the original one

   3. Work Involved: 

      1. new logic to set request amount:  RequestAmount should be set during the payment request, reporting should read requestAmount and requestDate from these two field.

      2. When process amount $-2000  is different from requested amount $-5000, new payment recrod with remaining amount -$3000 PENDING  should be created, two paymentId shall be linked (TBD)

         

7. Decrement Escrow -$2000

8. Withdrawal Table - Withdrawal $2000, Completed <--- ProcessDate Day1

9. WithdrawalRequest $5000 -> $3000

10. Day 2 - Back Office Canceled WD 

11. Payment CANCEL, Amount still $5000 (X)

### Payment

- Record the original request
- Aggregates all the payment details
  - will include first, last activity etc
  - converison rate, amount, etc

### Payment Details 

Payment details reflect what actually took place. it is the FACT

- The details, including rollback, break down etc.
- Model fee either in payment or payment details
- Withdrawal Tax also needs to be included 
- Rollback, WD Cancel etc

### Account Tran

- Account Tran <-> Payment Details.    <----->    Payment 

Withdrawal -> deduct balance -> account_tran

- 



## Payment Review (New*)

### Payment Review Design

When payment has been received, system will check the amount differnce between request and process, i.e.  [**Difference** = Request - Processed]. if **|Difference| >= |Threshold|**,  payment status would be set to  **IN REVIEW**.

- Threshold Settings
  - Per Brand, Currency, Method, Sub Method, Type [Deposit | Withdrawal]

---

## Payment Escrow 

### What is escrow 

**Escrow**  is a legal concept describing a financial instrument whereby an asset or escrow money is held by a third party on behalf of two other parties that are in the process of completing a transaction. Pending withdrawals, annonemous winnings without bet etc can be treated as escrow whereby staff review is necessary to approve and relesae the funds.

If a player's account is freezed, technically the money is in escrow.

### Escrow for payment

Escrow represents the funds currently on hold on behave of player, it is non-withdrawalable, non-playable. 

### Buckets of fund

Techncially speaking, escrow is applicable to all the buckets, Real, Released Bonus or Playable Bonus



----



payment details:

- PENDING - initial state
- REJECTED - Operator|Staff rejected  
- CANCELED - Player cancels the payment
- DECLINED - Provider declined the payment
- ROLLBACK
- ADJUSTED - requested is different from processed
- REVIEWED - making the preivous declined payment valid. (PIQ, Praxis) Provider changed the mind, want to do adjustment. give them the mean to update the payment. when you change the failed payment to successful. 
  - There has been no review request, shouldnt have reviewed status, it is confusing. we probably want to name it something else

Deposit 100, Bonus supposed to be 100. 

but if payment been declined, then there is no bonus.

but if payment got adjusted, the bonus still needs to be given, as indicated in the previous transactions, and adjust limits etc

| Amount | Action (Payment Detail & Status) + ProviderTransctionID      | Payment Status     |      |      |
| ------ | ------------------------------------------------------------ | ------------------ | ---- | ---- |
| -5000  | Request Withdrawal -5000, payment detail, <-5000>  Request Amount v.s. Actual Amount | Request            |      |      |
| +2000  | payment detail=COMPLETED Processed 2000                      | Process Partial    |      |      |
| +2000  | payment detail=COMPLETED Processed 2000                      | Process Pending    |      |      |
| 0      | payment detail=REJECTED 0 , amount 0                         | In Process         |      |      |
| +1000  | payment detail = COMPLETED amount 998 (**Review**)           | Completed          |      |      |
|        |                                                              | Completed - Review |      |      |

Payment detail 1: -2000
Payment detail 2: -2000
Payment detail 3: -1000x
Payment detail 3 rollback: +1000 (/)

user requests 1000, provider 998

Payment **Requested Amount** = **1000**

payment details

| Request Amount | Provider Actual Amount | ProviderTranID | Provider Status                      | OMEGA Status       |
| -------------- | ---------------------- | -------------- | ------------------------------------ | ------------------ |
| 1000           | **998**                | 111111         | **Pending \| Completed \| Declined** | Additional Action? |
|                |                        |                |                                      |                    |
|                |                        |                |                                      |                    |



Deposit Reuqest -> Approved -> In Process -> Comleted | Partially Completed | Failed

Withdrwal Requested -5000 ->  (Player) Escrow 



-----



## PaymentEscrow (Current Liability)

This payment escrow does imply that escrow been the middle man. Payment records all the fact without the need of this middle man. This is a calculated fields.

Payment escrow table is used to determine the current esscrow caused by withdrawal request. If a withdrawal request has not been finalized (COMPLETED, CANCELLED etc), and request amount is different from processed amount, then the record would be presented in this table.
This table shall be maintained by SQL Server Trigger ONLY to ensure data consistency.

Player escrow balance should be calculated from this table. Usage includes IPS API and Player Dashboard.

| Column                    | Type            | Description       |
| :------------------------ | --------------- | ----------------- |
| PaymentID                 | bigint          | ID of the Payment |
| PartyID                   | Integer         |                   |
| EscrowRealAmount          |                 |                   |
| EscrowReleasedBonusAmount | numeric(38, 18) |                   |
| EscrowPlayableBonusAmount | numeric(38, 18) |                   |

## Payment

| Column                    | Type            | Description                                          |
| :------------------------ | --------------- | ---------------------------------------------------- |
| PaymentID                 | bigint          | ID of the Payment                                    |
| PartyID                   | Integer         |                                                      |
| ParentPaymentID           | bigint          |                                                      |
| PaymentType               | varchar(30)     | DEPOSIT, WITHDRAWAL,                                 |
|                           |                 |                                                      |
| RequestPaymentMethod      | nvarchar(100)   |                                                      |
| RequestSubMethod          | nvarchar(100)   |                                                      |
| RequestIP                 | varchar(100)    |                                                      |
| RequestDevice             | varchar(30)     |                                                      |
| RequestDate               | datetime        |                                                      |
| RequestAmount             | numeric(38, 18) |                                                      |
| RequestedBonusPlan        | varchar(20)     | NONE, DEFAULT, or ID                                 |
| RequestContext            | nvarchar(max)   | Request payload,                                     |
| RequestContextType        | varchar(20)     | Voucher \| Manual bank                               |
| Status                    | varchar(30)     | Pending, InProcess, Completed, Failed                |
| ProcessedDate             | datetime        |                                                      |
| ProcessedAmount           | numeric(38, 18) | Total amount that is been processed                  |
| EscrowRealAmount          | numeric(38, 18) |                                                      |
| EscrowReleasedBonusAmount | numeric(38, 18) |                                                      |
| EscrowPlayableBonusAmount | numeric(38, 18) |                                                      |
| ShortResponse             |                 |                                                      |
| IsConverted               | bit             | Currency conversion required                         |
| TransactAmount            | numeric(38, 18) | Original transaction amount                          |
| TransactCurrency          | numeric(38, 18) | Original transaction currency                        |
| conversionRate            | numeric(38, 18) | TransactionCurrency / Player Currency Convrsion Rate |
| StaffID                   | Integer         | FK  to Staff StaffID                                 |
| CommunicationLogID        | bigint          |                                                      |



## PaymentDetail

Payment detail table is a payment **composition**. This table can be used for player payment history, which includes the full detail of payments and associated bonuses, including additional awards.

Only append operation shall take place in this table. **NO UPDATE ALLOWED.**

When deposit is made, a bonus associated with deposit would be triggered. In PaymentDetail table, one payment record for Type Real would be created, 1 payment record for Type Bonus would be generated based on the bonus been triggered. 

[TBD] if a bonus plan contains additional awards, then more bonus records can be linked and created here. 

When withdrawal is made, bonuses would be cancelled, it affects the account balance. One payment record for Type real would be created, N payment record for Type Bonus would be generated based on number of bonuses been affected.

when player request withdrawal, the cancellable playable bonuses could also be put into payment escrow [**future implementation**]

- Depending on the **playable bonus escrow rule** 
  - [**CURRENT DEFAULT IMPLEMENTATION**] if the withdrawal request should cancel cancellable playable bonuses immediately, then no playable bonus would be kept in payment escrow
  - [**FUTURE IMPLEMENTATION**] if only processed withdrawal should cancel cancellable playable bonus, then the bonuses that yet to be cancelled, should be kept in the payment escrow, and cancel bonus would take place during withdrawal process time by staff, then playable bonus in escrow would be removed. [Not Required Yet]

it records the various type of transactions took place when the actual transaction took place(except tax_defer), including account balance update, bonus balance update, fees been charged, tax been withheld, tax been deferred, and voucher used.

Each account tran id may or may not have the associated multiple payment detail  The relationship is <u>PaymentDetail</u> 0|N <----> 0|1 <u>AccountTran</u>.

E.x. If Withdrawal $100, $70 from Real, $30 from Released Bonus ($20 from Bonus1, $10 from Bonus2), it will also cancel $200 Playable Bonus from Bonus1, and $100 playble bonus from Bonus2.

it will generate 3 payment detail records for this withdrawal request, assuming escrow rule (need configuration) for bonus cancellation keeps the way it is now, which means playable bonus do not need to be put in escrow

<u>PaymentDetail</u>

- PaymentID=1, Type Real, AmountReal=-$70
- PaymentID=1, Type Bonus, BonusID=1, AmountReleasedBonus=-$20,  AmountPlayableBonus=-$200
- PaymentID=1, Type Bonus, BonusID=2, AmountReleasedBonus=-$10,  AmountPlayableBonus=-$100

<u>PaymentEscrow</u>

- PaymentID = 1,  EscrowRealAmount =-$70, EscrowReleasedBonus = -$30. EscrowPlaybleBonus=0 with the current default escrow rule. In the future, EscrowPlaybleBonus could be 300 if it is been configured

BonusID and relevant bonus amount is primarily used to keep track of bonus money been withdrawan for bonus accounting report. 

<u>Reporting Usage</u>

- Bonus Performance Report

- Bonus Accounting Report

- Withdrawal Tax Report

- 2000 

  - 1800
  - 200

- Rollback -2000

  - -1800
  - -200

  

| Column                      | Type                     | Description                                                  |
| :-------------------------- | ------------------------ | ------------------------------------------------------------ |
| PaymentID                   | bigint                   | FK to Payment PaymentID                                      |
| PaymentDetailID             | bigint identity(1,1)     |                                                              |
| ParentPaymentID             | bigint                   | If the tranaction completes without split, then ParementPaymentID = PaymentID, it gets set at the completion. |
|                             |                          |                                                              |
| Date                        | datetime not null        |                                                              |
| Type                        | varchar(30) not null,    | Real, Bonus, Fee, TaxWithheld (WD_TAX), TaxDeferred (no account_tran record), Voucher. more types can be added dynamically |
| Status                      | varchar(30)              |                                                              |
| BonusID                     | int                      |                                                              |
| AmountReal                  | numeric(38, 18) not null |                                                              |
| AmountPlayableBonus         | numeric(38, 18) not null |                                                              |
| AmountPlayableBonusWinnings | numeric(38, 18) not null |                                                              |
| AmountReleasedBonus         | numeric(38, 18) not null |                                                              |
| AmountReleasedBonusWinnings | numeric(38, 18) not null |                                                              |
| AccountTranID               | bigint                   |                                                              |
| Comments                    | nvarchar(max)            | Freespin details for bonus additional awards can be stored here |
| PK                          |                          | (PaymentID, PaymentDetailID)                                 |
|                             |                          |                                                              |



## CommunicationLog

| Column             | Type                 | Description      |
| :----------------- | -------------------- | ---------------- |
| CommunicationLogID | bigint identity(1,1) |                  |
| Provider           |                      |                  |
| EndPoint           |                      |                  |
| HostIP             |                      |                  |
| Device             |                      |                  |
| Status             | nvarchar(30)         |                  |
| ShortDescription   |                      |                  |
| RequestDate        | datetime             |                  |
| RequestContext     | nvarchar(max)        | Request Payload  |
| ResponseDate       | datetime             |                  |
| ResponseContext    | nvarchar(max)        | Response Payload |

---



Questions

Withdrrawal Request 100, tax rate 1% - Withheld

- PaymentEscrow 100
- AccountTran 2X - Withdrawal = 99, WD_TAX = 1

WR = 100 Tax rate 1% - defer 

- AccountTran 2X - Withdrawal = 100

read configuaration from brand/method/currency



- For Tax Deferred
  - What happen the withdrawal request took place, WD_TAX charged. Then the Tax Rate changed before withdrawal got approved?
- Payment Status mappings are different from provider to provider, how to record provider status?

# New Design #2

Can we do away from deducting money from player's account?

i.e. Withdrawal request simply doesn't create the transction in account_tran, only create the payment in pending like deposit

----

How Payment Works Today

- WithdrawalRequest would be deleted when payment is finished?
- PaymentIQ can cancel completed Withdrawal
- What is in bonus_id from WithdrawalRequest table?
- What is RUNNING status for withdrawal? as RUNNING Status can not be cancelled
- usp_CancelWithdraw will remove withdrawal request?
- 

## Escrow



| Column                     | Type            | Description      |
| :------------------------- | --------------- | ---------------- |
| EscrowID                   | int             |                  |
| PartyID                    | Integer         |                  |
| Date                       | datetime        |                  |
| Status                     | varchar(100)    | OPEN \| CLOSED   |
| RequestAmountReal          |                 |                  |
| RequestAmountReleasedBonus | numeric(38, 18) |                  |
| RequestAmountPlayableBonus | numeric(38, 18) |                  |
| AmountReal                 |                 | Processed amount |
| AmountReleasedBonus        | numeric(38, 18) |                  |
| AmountPlayableBonus        | numeric(38, 18) |                  |



## EscrowDetail



| Column                      | Type                     | Description                                                  |
| :-------------------------- | ------------------------ | ------------------------------------------------------------ |
| EscrowID                    | bigint                   | FK to Payment PaymentID                                      |
| EscrowDetailID              | bigint identity(1,1)     |                                                              |
| Date                        | datetime not null        |                                                              |
| Type                        | varchar(30) not null,    | Real, Bonus, Fee, TaxWithheld (WD_TAX), TaxDeferred (no account_tran record), Voucher. more types can be added dynamically |
| Status                      | varchar(30)              |                                                              |
| BonusID                     | int                      |                                                              |
| AmountReal                  | numeric(38, 18) not null | Processed amount                                             |
| AmountPlayableBonus         | numeric(38, 18) not null |                                                              |
| AmountPlayableBonusWinnings | numeric(38, 18) not null |                                                              |
| AmountReleasedBonus         | numeric(38, 18) not null |                                                              |
| AmountReleasedBonusWinnings | numeric(38, 18) not null |                                                              |
| AccountTranID               | bigint                   |                                                              |
| Comments                    | nvarchar(max)            | Freespin details for bonus additional awards can be stored here |
| PK                          |                          | (EscrowID, EscrowDetailID)                                   |

## EscrowBalance



| Column                     | Type            | Description |
| :------------------------- | --------------- | ----------- |
| PartyID                    | Integer         |             |
| BalanceReal                |                 |             |
| BalanceReleasedBonus       | numeric(38, 18) |             |
| BalanceAmountPlayableBonus | numeric(38, 18) |             |





WR 1000

- AcountTran WD_REQ Real  = **-1000**, Escrow Real **+1000**
- Escrow +1000
- When close the WD
  - AccountTran **Withdrawal** Escrow Real =-900
  - Account_Tran **WD_TAX** Escrow Real  = -100
- 
- 

Withdrawal Processed 900, TAX Withheld 100

