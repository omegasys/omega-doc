## Steps

### Backup the database

1. right click on <database> 

2. Task -> Backup ...

![backup-db-step-1.png](./images/backup-db-step-1.png)

3. Confirm the backup location and -> Ok

![backup-db-step-2.png](./images/backup-db-step-2.png)

### Download the .bak file to local

1. Compress the .bak file to .zip if the .bak file is too big
   ![database-backup-compress-1](images/database-backup-compress-1.png)

2. Copy and Paste the image to local mapped drive

   ![database-backup-copy-to-local](images/database-backup-copy-to-local.png)

### Copy the .bak from local to docker 

1. Bring up terminal and copy the .bak file from local machine to docker. Assumption: - The msql server docker container is called `mssql`

   ```shell
    cp ~/Downloads/omega_qa.bak mssql:/var/opt/mssql/backup/  
   ```

2. confirm the .bak file has been copied

   ```shell
   docker exec -it mssql ls /var/opt/mssql/backup
   ```

### Prepare the restore script

The restore script can technically be auto generated using `RestoreDatabase.sql` from omegadb project. A sample output script `RestoreDatabaseTemplate.sql`also been created using local docker env for reference. 

- Run RestoreDatabase.sql against docker image
  - typically with IP 127.0.0.1, port 1433
- It will generated the SQL output, thats similar to `RestoreDatabaseTemplate.sql`

### Execute the restore script

- **Assumption** 

- - The source database name is omega_qa, with backup file stored under `/var/opt/mssql/backup/omega_qa.bak`
  - The target database is named `omega_dev`
  - local docker mssql server image is called `mssql`
  - The script would be executed by super user sa

```sql
restore database omega_dev
from
    disk = N'/var/opt/mssql/backup/omega_qa.bak' with file = 1,
    move N'omega_qa' to N'/var/opt/mssql/data/omega_dev.mdf',
    move N'omega_qa_Report1' to N'/var/opt/mssql/data/omega_dev_Report1.ndf',
    move N'omega_qa_Report2' to N'/var/opt/mssql/data/omega_dev_Report2.ndf',
    move N'omega_qa_Report3' to N'/var/opt/mssql/data/omega_dev_Report3.ndf',
    move N'omega_qa_Report4' to N'/var/opt/mssql/data/omega_dev_Report4.ndf',
    move N'omega_qa_Report5' to N'/var/opt/mssql/data/omega_dev_Report5.ndf',
    move N'omega_qa_Report6' to N'/var/opt/mssql/data/omega_dev_Report6.ndf',
    move N'omega_qa_Report7' to N'/var/opt/mssql/data/omega_dev_Report7.ndf',
    move N'omega_qa_Report8' to N'/var/opt/mssql/data/omega_dev_Report8.ndf',
    move N'omega_qa_Data1' to N'/var/opt/mssql/data/omega_dev_Data1.ndf',
    move N'omega_qa_Data2' to N'/var/opt/mssql/data/omega_dev_Data2.ndf',
    move N'omega_qa_Data3' to N'/var/opt/mssql/data/omega_dev_Data3.ndf',
    move N'omega_qa_Data4' to N'/var/opt/mssql/data/omega_dev_Data4.ndf',
    move N'omega_qa_Data5' to N'/var/opt/mssql/data/omega_dev_Data5.ndf',
    move N'omega_qa_Data6' to N'/var/opt/mssql/data/omega_dev_Data6.ndf',
    move N'omega_qa_Data7' to N'/var/opt/mssql/data/omega_dev_Data7.ndf',
    move N'omega_qa_Data8' to N'/var/opt/mssql/data/omega_dev_Data8.ndf',
    move N'omega_qa_log' to N'/var/opt/mssql/data/omega_dev_log.ldf',
    replace,
    stats = 1
go
    -- The follow script will fix the admin_all user default schema mapping after database restore
EXEC sp_change_users_login 'Auto_Fix', 'admin_all', NULL, 'admin_all'
go
```

