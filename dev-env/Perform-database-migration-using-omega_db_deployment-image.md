### Perform Database Migration

To perform database migration, please ensure that 

- <tag name> tag name is typically the release tag, such as `4.14.4`
- <Real internal IP address of the database> below been replaced by the actual IP address or name of the database server. If you are running local docker instance, then the docker mssql IP address would typically be the IP address been assigned to your machine. Use `ifconfig` to identify such IP. typically the IP would be 192.168.x.x or 10.x.x.x or 172.x.x.x
- <database name> is the name of the database, for dev enviornment, it is typically omega_dev
- <database sa user> is typically `sa`. We will need super user in order to create certain jobs. `admin_all` will NOT work.
- <dataabase password> the password of the user

```shell
docker run --rm "docker.io/omegasystems/omega_db_deployment:<tag name>" install.py <Real internal IP address of the database>,1433 <database name> <database sa user>  <database password>
```

example:

```shell
docker run --rm "docker.io/omegasystems/omega_db_deployment:4.14.4" install.py 10.0.0.191,1433 omega_dev SA Omega123
```

