## **Exlore MetaMask**

### Pre-Requisite 

### Definition

#### What is DeFi (Decentralized Finance)?

Decentralized finance (Defi) is an emerging financial technology based on secure distributed ledgers similar to those used by cryptocurrencies. 

- Decentralized finance, or DeFi, uses emerging technology to remove third parties and centralized institutions from financial transactions.
- The components of DeFi are **stablecoins**, **software**, and **hardware** that enables the development of applications.
- The infrastructure for DeFi and its regulation are constantly evolving.

##### Reference 

[https://www.investopedia.com/decentralized-finance-defi-5113835]





#### Setup MetaMask Account

- From Chrome Extension - Create Account
  - ![image-20221002082706305](/Users/zhengchen/Dev/omega-doc/integration/MetaMask/Setup-MetaMask-Account-20221002082706305.png)
  - The secret recovery phrase is generated, need to be stored safely, it is too powerful, and can be used to do anything with your account
  - 





- 