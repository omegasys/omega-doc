#### Attention!

- When a user gets an address for the first time they are able to save it into their crypto wallet application. It is more comfortable for the user because they don’t need to go to the "Cashier" section every time when they want to make a deposit. That is why many users save the address in their crypto wallet application after the first deposit and then they don't need to go to the payment form every time.
  - Deposit Limit?
  - Even if you show the users new addresses every time, they can always **make the deposit to the older one**, and they expect that they will be able to see their funds on your website. 
    - Address must be migrated from the OLD to the NEW
- **IMPORTANT**
  - Notify the user about exchange rates, 
  - deposit limits 
    - [Deposit and withdrawal limits]()
  - and the chosen cryptocurrency
  - <u>**WARNING**</u>
    - All deposits below the limits or sent to incorrect addresses will be lost
    - send the funds using a specific cryptocurrency to the address from a **different blockchain**. These funds will be **lost** in most cases. The funds could be particularly restored according to the "Crosschain recovery policy": [Crosschain recovery policy]()



#### Attention

- Unlike the "**/v2/addresses/take**" method, you should use the **unique "foreign_id"** every time you make a withdrawal request to our system. 

For Withdrawal

- Player requested withdrawal,

- - If you need to calculate exact amounts corresponding to current exchange rates try the IPS API prepareDeposit?action=EXCHANGE_CALCULATE method (similar to coinspaid /v2/exchange/calculate)
- In case you would like to hold the exchange rate for 10 minutes in order to receive a specified amount from the user after converting the funds, you may want to use these methods: OMEGA IPS API prepareDeposit?action=GET_FUTURES_RATES (similar to coinspaid /v2/futures/rates) , prepareDeposit?action=FUTURES_CONFIRM (similar to coinspaid /v2/futures/confirm)





n request ={"id":849509,"foreign_id":"16","type":"withdrawal","crypto_address":{"id":1811040,"currency":"BTC","address":"tb1qveh2ujg4rs8h637yu9nmxdcs0ufasswth8xwcw","tag":null},"currency_sent":{"currency":"BTC","amount":"0.00100000"},"currency_received":{"currency":"BTC","amount":"0.00100000"},"transactions":[{"id":856420,"currency":"BTC","transaction_type":"blockchain","type":"withdrawal","address":"tb1qveh2ujg4rs8h637yu9nmxdcs0ufasswth8xwcw","tag":null,"amount":"0.00100000","txid":"3544c93693696c3ab06de24e7ceaff7b883bb30fc9bec9548d2b94e26c6223ce","riskscore":null,"confirmations":"0"}],"fees":[{"type":"mining","currency":"BTC","amount":"0"}],"error":"","status":"confirmed"} 

2022-12-08 06:23:32.236 [http-nio-8081-exec-2] INFO c.o.c.p.c.PaymentServiceImpl isValidCallbackIPAddress ipAddress=18.184.195.1 serviceName=coinspaid

2022-12-08 06:23:32.302 [http-nio-8081-exec-2] INFO c.o.p.w.s.p.CoinspaidCallbackActionBean apiKey from Coinspaid= Jw7MWrvg5IfFERfIzIbMXtoCvfss040N

2022-12-08 06:23:32.304 [http-nio-8081-exec-2] INFO c.o.p.w.s.p.CoinspaidCallbackActionBean Coinspaid withdraw come back PaymentId = confirmed and status = 16

2022-12-08 06:23:32.305 [http-nio-8081-exec-2] WARN c.o.p.w.s.p.CoinspaidCallbackActionBean Payment status is Cancelled, no pending. So just ignore it.

2022-12-08 06:23:32.306 [http-nio-8081-exec-2] INFO c.o.l.u.w.URLUtil   getParameterString: 

2022-12-08 06:23:32.310 [http-nio-8081-exec-2] INFO c.o.l.b.s.LibApiServiceLogService Inserting Api Service Log:ApiServiceLogDto[id=0, partyId=91429892, brandId=9, providerKey=COINSPAID, input={"id":849509,"foreign_id":"16","type":"withdrawal","crypto_address":{"id":1811040,"currency":"BTC","address":"tb1qveh2ujg4rs8h637yu9nmxdcs0ufasswth8xwcw","tag":null},"currency_sent":{"currency":"BTC","amount":"0.00100000"},"currency_received":{"currency":"BTC","amount":"0.00100000"},"transactions":[{"id":856420,"currency":"BTC","transaction_type":"blockchain","type":"withdrawal","address":"tb1qveh2ujg4rs8h637yu9nmxdcs0ufasswth8xwcw","tag":null,"amount":"0.00100000","txid":"3544c93693696c3ab06de24e7ceaff7b883bb30fc9bec9548d2b94e26c6223ce","riskscore":null,"confirmations":"0"}],"fees":[{"type":"mining","currency":"BTC","amount":"0"}],"error":"","status":"confirmed"}, output=, url=https://ps.omegasys.eu/ps/pub/CoinspaidCallback.action?, path=, httpStatus=0, startDate=2022-12-08T06:23:32.236, duration=69, result=SUCCESS]