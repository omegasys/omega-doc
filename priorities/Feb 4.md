

## TODO 

- [ ] PaymentIQ Configuration Documentation and Demo
- [ ] BT Cancelbet from back office

### **Zheng**

### Leo 
- [ ] BIA SB2

BW

- ETL Application as AWS Replication

### Shiwei
- [x] MC Test Plan 
- [ ] Wow Vegas Dev Testing - PIQ
- [ ] Virtual Currency shall not be account for for RG
- [ ] MC Reporting - if it is virtual currency

**Wenbo**

- [ ] **Clear Support Item First**
  - [ ] FF-347
  - [ ] MB-378 Portal Reporting a little off, could be reading from different tables
  - [ ] APGL-365 Uploaded By Player is been set to Player even when the document is NOT been uploaded by Player
  - [ ] APGL-364 Bugfix for Misleading Shufti Document attribute

**Fanny**

- [x] Player Migration API 1.0
  - [ ] CG-7, FF-315 Player Migration API v1.2 
- [ ] C5-1586 OpenJDK certificate Support for installation script
- [ ] ID-228 Trustly Integration 

**Mak**

- [ ] VP-379 Egamings Update
- [ ] VP 4.15.x Deployment
- [ ] Mars support enhancement while Mak is hot?

Anmol - Talk to him

### DB Maintenance (Dry Run) Status

- [x] BW Prod
- [x] LBX STG Prod
- [x] VIP STG Prod
- [x] YYY
- [ ] BW STG
- [ ] MOZ STG, PROD
- [ ] Others?

### Release

- **Week of Jan 18  - 4.16.0 RC (Shall be released to client staging)**
  - 4.17.0 Develop Branch created  **(Feb 7th to Feb 28th)**
  - **deploy  4.16.0 release to STG**
- Jan 16th - Replicate View (**Reg Feature**) -> Hotfix 4.16.1/2/3
- BW Production
- WOW (new client) 
  - **4.17.0** **-snapshot** (Jan 27- Jan 31) 3 **Major Bonus** Features 
- **GCH** (4.15.x STG -skip 4.16.x-> **4.17.0**) Jan 31?--> need to confirm with Ailbhe 
  - Player Migration Feature
  - p7s signed exports (Mak)
  - Reg Features (4.17.0)
- **BETTECH** (4.14.x -> 4.? lateat version) End of Feb
  - Add payment to account tran
  - Meeting with BetTech, more items to come

### WOW

- Additional Awards (/)
- Signup Bonus (/)
- MultiCurrency IPS API Test (6 D)
  - Which IPS API can support primary account session key, which should support associated account session key, validation is required at the interceptor level
- Virtual Currency should not count towards RG limits (Future)
- Vegas Bucks are Virtual Currency - configuration 
- Wow Coins would be treated as Fiat Currency - configuration 
- Player Dashboard - Overall Needs to go away ? W/S/L



### Payment

- Concept of Escrow
  - Use this to give people the aiblity to hold the funds on behave of a player, 
  - not make them accessible
  - for player review
  - Anonoous win 
    - option to place winning funds to escrow account
    - staff has the ability to see the escrow funds
- Payment Withdrawal Life Cycle Review
- Payment Reporting Review



Configuration and Validation

- Have to exclusion database for player, CRUKS database
- Self exlcude player there needs to be a report, which means player needs to be tagged
- Payment workflow Diagram

Wow Coins, and Vegas Bulks --> Betsoft

- Document for multi-currency







SQL Replication 
Distributed transaction in SQL Server has heavy dependencies on NETWORK, and it is very fragile component that we should avoid using.
Maintenance overhead is extremely high as any DDL change would have to be manually synced. 
Not commended option at all.



Sharding for performance

- All in 1 database center
- design routing application 
- application talk to routing application 
- routing application direct traffic to specific db instance 
- Reporting would be on main database 
- Main database will have to pull transactions from the rest of db instance and aggregate from there









TG Lab

- negative balance
- bonus discussion
- portal3.omegasys.eu
- credentials? Shared Key

- ETL
- Hourly Succeful Notification 

- 