# Terminologies 

## Transit Gateway 

A **transit gateway** is a network **transit** hub that you can use to interconnect your virtual private clouds (VPC) and on-premises networks. For more information, see AWS **Transit Gateway** .

## CloudFront (CDN)

Like CDN, will not use cloudflare