## Cisco AnyConnect VPN with Bash

### Connecting to VPN Server

```bash
/opt/cisco/anyconnect/bin/vpn connect ciscossl.mozzartbet.com
```

### Enter the username:

```bash
Copyright (c) 2004 - 2019 Cisco Systems, Inc.  All Rights Reserved.


  >> state: Disconnected
  >> state: Disconnected
  >> notice: Ready to connect.
  >> registered with local VPN subsystem.
  >> contacting host (ciscossl.mozzartbet.com) for login information...
  >> notice: Contacting ciscossl.mozzartbet.com.

  >> Please enter your username and password.

Username: [zheng.chen] 

```

### Enter the password:

```bash
Password: [the real password]
  >> state: Connecting
  >> notice: Establishing VPN session...
  >> notice: The AnyConnect Downloader is performing update checks...
  >> notice: Checking for profile updates...
  >> notice: Checking for product updates...
  >> notice: Checking for customization updates...
  >> notice: Performing any required updates...
  >> notice: The AnyConnect Downloader updates have been completed.
  >> state: Connecting
  >> notice: Establishing VPN session...
  >> notice: Establishing VPN - Initiating connection...
  >> notice: Establishing VPN - Examining system...
  >> notice: Establishing VPN - Activating VPN adapter...
  >> notice: Establishing VPN - Configuring system...
  >> notice: Establishing VPN...
  >> state: Connected
```

Disconnect VPN

```
/opt/cisco/anyconnect/bin/vpn disconnect 
```

