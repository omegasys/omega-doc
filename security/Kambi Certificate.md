# Certificate generation

1. The Operator needs to generate a SSL certificate signing request (**CSR**
2. Send CSR to Kambi, who signs it and provide a signed client certificate to the operator
3. How to generate CSR and test the certificate
   - The DEV-CSR.csr and PROD-CSR.csr files generated should then be sent to Kambi IT Operations for signing
   - after which a client certificate will be sent back to the Operator
   - OMEGA Application access Kambi API with client certificate packaged for authentication



**Important** 

- **Do not** send the private key named DEV-nopass-key.pem and/or PROD-nopass-key.pem in the examples, they should remain private and secret to Kambi and the world.
- Ensure the "Common Name" field is fully populated. The name (CN) is in the form "[CUSTOMER NAME] Development certificate" or "[CUSTOMER NAME] Production certificate" depending on the environment.
- Email address must contain the address we can contact when the certificate expire.

### **For the development setup:**

```
#
# No value needed for the extra attributes including password
#
openssl req -sha1 -out DEV-CSR.csr -new -newkey rsa:2048 -nodes -keyout DEV-nopass-key.pem
Generating a 2048 bit RSA private key
........+++
.........+++
writing new private key to 'DEV-nopass-key.pem'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:MT
State or Province Name (full name) [Some-State]:[Put the state here]
Locality Name (eg, city) []:[Put the city here]
Organization Name (eg, company) [Internet Widgits Pty Ltd]:[PUT THE CUSTOMER NAME HERE]
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:[PUT THE CUSTOMER NAME HERE] Development certificate
Email Address []: [security@customer.com]
Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:
```

### **For the production setup:**

```
#
# No value needed for the extra attributes including password
#
openssl req -sha1 -out PROD-CSR.csr -new -newkey rsa:2048 -nodes -keyout PROD-nopass-key.pem
Generating a 2048 bit RSA private key
........+++
.........+++
writing new private key to 'PROD-nopass-key.pem'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:MT
State or Province Name (full name) [Some-State]:[Put the state here]
Locality Name (eg, city) []:[Put the city here]
Organization Name (eg, company) [Internet Widgits Pty Ltd]:[PUT THE CUSTOMER NAME HERE]
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:[PUT THE CUSTOMER NAME HERE] Production certificate
Email Address []: [security@customer.com]
Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:
```

# Test the certificate

To be able to test the certificate please use curl or openssl. The simple test can be done with curl and a bit more complex case with openssl when investigating certificate issues. The cacert-v2.pem file can be found attached to this article. Examples can be found below: 

If the curl is successful you will get some sort of output (could even be an HTTP error).  
If the openssl test is successful you will get an connect and can do a " GET / HTTP/1.1" and get a application server error

### For customers connecting via internet:

**Curl example**

```
curl --cert cert.pem --key key.pem --cacert cacert-v2.pem https:``//ctn-dwh.api.kambi.com/datawarehouse-api/punters/2013-09-25T00:00:00/2013-09-25T00:30:00
```

### For VPN customers:

**Curl example**

```
curl --cert cert.pem --key key.pem --cacert cacert-v2.pem https:``//ctn-api.dwh.kambi.com/datawarehouse-api/punters/2013-09-25T00:00:00/2013-09-25T00:30:00
```

### For customers connecting via internet:

### **Openssl example**

```
openssl s_client -connect ctn-api.dwh.kambi.com:``443` `-CAfile ./cacert-v2.pem -cert cert.pem -key key.pem
```

# Troubleshoot the setup

First of all, make sure your system have the right time and date, NTP sync the time as its critical for certificates.

### When troubleshooting the setup you need to have openssl installed, if the test with curl fails and you get errors such as:

**curl errors**

```
curl https://ctn-dwh.api.kambi.com/datawarehouse-api/punters/2013-09-25T00:00:00/2013-09-25T00:30:00
curl: (60) SSL certificate problem, verify that the CA cert is OK. Details:
error:14090086:SSL routines:SSL3_GET_SERVER_CERTIFICATE:certificate verify failed
More details here: http://curl.haxx.se/docs/sslcerts.html
  
curl performs SSL certificate verification by default, using a "bundle"
 of Certificate Authority (CA) public keys (CA certs). If the default
 bundle file isn't adequate, you can specify an alternate file
 using the --cacert option.
If this HTTPS server uses a certificate signed by a CA represented in
 the bundle, the certificate verification probably failed due to a
 problem with the certificate (it might be expired, or the name might
 not match the domain name in the URL).
If you'd like to turn off curl's verification of the certificate, use
 the -k (or --insecure) option.
```

### That means you missed the arguments to use the certificates, if you get errors such as:

**curl errors**

```
curl --cert cert.pem --key key.pem --cacert cacert-v2.pem https://ctn-dwh.api.kambi.com/datawarehouse-api/punters/2013-09-25T00:00:00/2013-09-25T00:30:00
curl: (60) SSL certificate problem, verify that the CA cert is OK. Details:
error:14090086:SSL routines:SSL3_GET_SERVER_CERTIFICATE:certificate verify failed
```

That means you are using a certificate that is not valid and signed by Kambi. Please supply the output from openssl x509 -in cert.pem -text

Any HTTP errors given means that the certificate is working fine and it could be an issue with the parameters you are using or the application server it self.

# Update

Previously, the certificate bundle was named cacert.pem, which is now decomissioned due to that it expires Feb 4, 2021. 

The new cacert-v2.pem must be used instead.

If you have a certificate issued before January 14, 2021, please send us a new Certificate Signing Request (CSR). 

***Certificates issued before January 14, 2021 will stop working February 4, 2021.\* **

**[cacert-v2.pem](../../../Downloads/cacert-v2.pem) **