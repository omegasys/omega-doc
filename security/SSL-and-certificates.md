## Overview

### What is SSL

**Secure Sockets Layer** (SSL) and **Transport Layer security** (TLS ) are protocols that provide secure communications over a computer network or link. They are commonly used in web browsing and email.

### What is TLS

**TLS** is based on **SSL** and was developed as a replacement in response to known vulnerabilities in SSLv3. SSL is the term commonly used, and today usually refers to TLS.

### Data Encryption, Data Integrity and Authentication

SSL/TLS provides data encryption, data integrity and authentication.

This means that when using SSL/TLS you can be confident that

- No one has read your message
- No one has changed your message
- You are communicating with the intended person (server)

When sending a message between two parties you have two problems that you need to address.

- How do you know that no one has read the message?
- How do you know that no one has changed the message?

The solutions to these problems are to:

- **Encrypt it.**– This makes the content unreadable so that to anyone viewing the message it is just gibberish.
- **Sign it**– This allows the recipient to be confident that it was you who sent the message, and that the message hasn’t been changed.

#### Encryption and Signature processes require the use of keys.

These keys are simply numbers (128 bit being common) that are then combined with the message using a particular method, commonly known as an algorithm- e.g. RSA, to either encrypt or sign the message.

##### Symmetrical key

Symmetrical key is used to encrypt or sign the message, and the **same key** is used to decrypt the message. This is the same as the keys (door, car keys) we deal with in everyday life.

The problem with this type of key arrangement is if you lose the key anyone who finds it can unlock your door.

##### Public and Private Key

- Needs to be in pair,  mathematically related 
- Public key **encrypts** content. 
  - This means a message **encrypted with a public key** cannot be **decrypted with the same public key**.
- Private key **decrypts** the content
- If this type of key arrangement were used with your car. Then you could lock the car, and leave the key in the lock as the same key **cannot** unlock the car.

> This type of key arrangement is very secure and is used in all modern encryption/signature systems.

### Keys, SSL Certificates and Certificate Authority (CA)

SSL/TLS use **public and private key** system for data encryption and data Integrity. Public keys can be made available to anyone, hence the term **public.**

**Digital certificate** is like a Passport, or ID that was issued by a trusted certificate authority (**CA**) / organization such as gonverment, that <u>certifies</u>  a particular public key belongs to the entity (business, domain name etc) that it claims to be, and has been verified (by signing) by the certificate authority (**CA**)

A **digital certificate** provides a convenient way of distributing **trusted public encryption keys**.

### Obtaining a Digital Certificate

You get a digital certificate from a recognized **Certificate authority (CA).** Just like you get a passport from a passport office.

In fact the procedure is very similar.

1. **Certificate Request**: You fill out the appropriate forms **add your public keys** (they are just numbers) and **send it/them to the certificate authority**. (this is a **certificate Request**)

2. **Certificate Signed by CA**: The certificate authority does some checks ( depends on authority), The certificate is **signed** by the **Issuing Certificate authority**, and this is what guarantees the keys.
3. **Create Certificate**: The CA sends you back the **keys** enclosed in a **certificate**.

4. **Share Public Keys**: Now when someone wants your public keys, you send them the certificate, they **verify the signature** on the certificate, and if it verifies, then they can **trust your keys**.

Reference

- [Chinese] https://mp.weixin.qq.com/s/XWdPVQlHSbQb3Q88VyH2dg
- [English] http://www.steves-internet-guide.com/ssl-certificates-explained/