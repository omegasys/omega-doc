## Certificate Generation

The manager needs to generate a GDPR certificate for the staff first. The manager himself doesn't need a certificate to do so.

1. Pick a staff that does not have an **active** certificate
![](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/generate%201.png)


2. Generate a GDPR certificate using the manager's secret Key
![](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/generate%202.png)


3. Once the certificate is generated, an **initial password** is returned to the dialog. 
Manager needs to send this password to the staff so that he can activate the certificate later.
![](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/generate%203.png)


4. A certificate becomes **PENDING** when a manager has granted to a staff but the staff has not activated and set his own password.
![](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/generate%204.png)

## Certificate Activation


1. A staff can activate his granted GDPR certificate after being given the initial password. The button locates in the dropdown menu under Staff's username.
![](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/activate%201.png)


2. The initial password is from the manager, while the certificate password is chosen by the staff. It can be whatever is meaningful to him.
![](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/activate%202.png)


3. Once a certificate is activated, the staff can use it to perform **GDPR Delete** or **Restored** depending on the given permissions. The **Certificate Password** can still be changed after activation. 
![](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/activate%203.png)


## GDPR Delete and Restore 

1. From player's dashboard, there's a highlighted button, **GDPR Delete Lv1**. Staffs with GDPR certificate can use this feature to safely encrypt player's personal information.
![](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/action%201.png)


2. After encryption, the player's account status becomes **GDPR Delete Lv1**
![](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/action%202.png)


3. Check Player's Profile and see all the personal data is encrypted
![](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/action%203.png)


4. After a player's account is encrypted, the staff is presented with the other 2 GDPR actions, **Restore** or fully **Anonymize** the encrypted data.
Restore or anonymize follows the same workflow as demonstrated in step 1.
![](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/action%204.png)


5. The GDPR action histories can be checked in user's action log. 
![](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/action%205.png)

## Permission Control

GDPR certificate is required to perform **GDPR Delete** and **Restore**. But even with the certificate staff may still be blocked from performing certain actions.

For example, if **GDPR Restore** is not desired, the administrator can uncheck this node from the role tree so that staff with GDPR certificate can only do **GDPR Delete Lv1** and **GDPR Delete Lv2**
![](https://raw.githubusercontent.com/chenzhengbc/md-images/develop/uPic/permission.png)
