July 19th

- LIVE(Prod) Testing need 3 Test Users with NO PREVIOUS ACTIVITY
  - Need to make 1st UA Call
  - CAN NOT PLAY ANY GAME
  - EVO Needs to change User Type Manually in their system

July 20th

- Milos sent the test accounts in private msg to Eglis (which accounts we do not know)
- Evo  confirmed they were ready just need to enable tables on production
- Milos is waiting for testing a game on Staging
- Milos asked for credentials on Prod(Live)
  - Egils - https://livecasino.mozzartbet.com/ua/v1/mozzartbet000001/ec123c5cdd682e0bf020aa12b552c2565e32c688
    -  https://livecasino.mozzartbet.com/
      Casino ID = mozzartbet000001
    - API token - ec123c5cdd682e0bf020aa12b552c2565e32c688
  - Got error with bad end point configuration (extra https)

July 21st

- Yan pudated the end point, but got the .UnknownHostException from Evo 
- Evo & Nemanja updated the hostname to mozzartbet.evo-games.com. Different from   https://livecasino.mozzartbet.com/ they orginally provided (problem on Evo)
- After the update, there are wallet IP addresses still need to be whitelisted
  - Cedomir provides the IP, but it is still having issues
  - The IP Address to be whitelisted should be documented in the wiki (for easy communication going forward)

July 22nd

- Not much we can do, provide more ip addresses hoping that could help

July 23rd

- IP address issue solved. 
- Evo asked us to test
- We got Invalid Token ID error during UA (User Authentication)
- Eglis said OMEGA ARE RETURNNING WRONG STATUS
  - OMEGA called this API https://{hostname}/ua/v1/{casino.key}/{api.token}
  - OMEGA received {"status":"INVALID_TOKEN_ID","sid":null}



July 26th

- Leo and Robbie started the investigation 
  - Robbie confirmed that Eglis provided false statement, FULL Request and Response Log were provided 
    - OMEGA attempted to reach EVO API https://mozzartbet.evo-games.com/ua/v1/mozzartbet000001/ec123c5cdd682e0bf020aa12b552c2565e32c688 to perform User Authentication
    - INVALID TOKEN_ID error was given by EVO API Response 
  - Asked Evo to confirm the token is indeed valid

July 27th

- Eglis mentioned  As for the error, this is from API documentation :
  G.8	Unable to authenticate user due to: $status	Most likely client system returned invalid $status.
  - He further provided the detailed log
  - Checked logs, on OW call you respond with :
    Status: 200 OK
    Headers: Date: Fri, 23 Jul 2021 12:23:34 GMT; Connection: keep-alive; CF-Cache-Status: DYNAMIC; Expect-CT: max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"; Server: cloudflare; CF-RAY: 6734e17adf754e3d-FRA
    Content type: application/json
    Body length: 40
    Body: {"status":"INVALID_TOKEN_ID","sid":null}
  - ZC: The above log would mean that the error is probably come from wallet?? when the Evo tries to create the user, it might make an API call to wallet to confirm the info about the user before creation. 
  - OMEGA Wallet will return invalid token error, if the token is not been configured properly under Platform Table. THIS WAS NOT BEEN DOCUMENTED PROPERLY SO NONE DEVELPERS DO NOT KNOW WHERE TO CONFIGURE

![image-20210728062949185](image-20210728062949185.png)

Do not share private IP address above





[omega@sl-txs1 ~]$ cat logs/omegaTron.log | grep "2021-07-28 16:51:0[34].* \[http-nio-8081-exec-10209\]"

2021-07-28 16:51:04.152 [http-nio-8081-exec-10209] INFO LogFilter      [PARTYID=] Start request :: ipAddress=91.213.212.38, uri=txs.mozzartbet.com/omegatron/spr/EvoWallet/check, params=[authToken=EVO_CAS]

2021-07-28 16:51:04.152 [http-nio-8081-exec-10209] INFO RegistryService   [PARTYID=] Retrieving Registry Hash blacklist.enabled from DB...

2021-07-28 16:51:04.156 [http-nio-8081-exec-10209] INFO RegistryService   [PARTYID=] Warning :: No value found for key blacklist.enabled

2021-07-28 16:51:04.156 [http-nio-8081-exec-10209] INFO PlatformService   [PARTYID=] Platform not found for username=EVO_CAS ,password=EVO_CAS

2021-07-28 16:51:04.156 [http-nio-8081-exec-10209] ERROR EvoWalletController [PARTYID=] Authentication failed, reply={"status":"INVALID_TOKEN_ID","sid":null}

2021-07-28 16:51:04.156 [http-nio-8081-exec-10209] INFO LogFilter      [PARTYID=] End request (4ms) (ssw-nullms):: /omegatron/spr/EvoWallet/check:4 :: ipAddress=91.213.212.38, uri=txs.mozzartbet.com/omegatron/spr/EvoWallet/check, params=[authToken=EVO_CAS]

