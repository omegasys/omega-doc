The UIF (FIU) is a special governmental agency responsible for monitoring compliance with the Anti-Money Laundering Law. The Financial Information Unit of the Argentine Republic (UIF) is a body with financial autonomy and autarky, which is in charge of the analysis, treatment and transmission of information in order to **prevent Money Laundering (LA)**, the **Financing of Terrorism (FT)**, and **Complex Economic-Financial Crimes (DEC**). The activity of the FIU is developed around the control, detection, investigation and punishment of ML / FT / DEC crimes in order to contribute to strengthening the financial system and the safeguarding of the socio-economic order.

### Who are the Obliged Subjects?

https://www-argentina-gob-ar.translate.goog/uif/sujetos-obligados/listado?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=ajax,elem

They are the Natural and Legal Persons indicated in article 20 of Law No. 25,246 and its amendments. The sectors determined in said article are those that the legislators considered **vulnerable** for **Money Laundering** and **Terrorism Financing**.

If you are Obliged Subject, you must complete the registration ON LINE in the Operations Reporting System (SRO), through the button "Register for the first time", and then submit the supporting documentation for said registration, within 15 business days. to your ONLINE registration. 

```json
{
      "index": "ldi",
      "id": "6OPpPXsBdk30_Q6utqhJ",
      "score": 100,
      "fuente": "UIF_SO",
      "tipoFuente": "SO",
      "descripcionFuente": "Sujetos Obligados reglamentado por la UIF",
      "sdnId": "14250892",
      "fechaNovedad": "2020-11-09T00:00:00",
      "nombre": "MACRI JORGE HORACIO",
      "documento": null,
      "identificacionTributaria": "20134301652",
      "institucion": null,
      "cargo": "Los agentes o corredores inmobiliarios matriculados.",
      "relacion": null,
      "domicilio": null,
      "residencia": "AR",
      "descripcionPais": "Argentina",
      "url": "http://info.pld.global/Base/Index?referencia=VUlGX1NPfDE0MjUwODky",
      "evaluacion": 2,
      "existeDocumentacion": false
    }
```


```json
"resultados": [
  {
    "id": 72539,
    "fuente": "B_PEPS_SI_AR",
    "fuenteDescripcion": "PEP de Argentina sin identificación",
    "sdN_ID": "14124078",
    "nombreModificado": "SERGIO BERNI",
    "documento": "",
    "codigoTipoFuente": "PEP",
    "tipoFuente": "P.E.P.",
    "residencia": "AR",
    "descripcionPais": "ARGENTINA",
    "paisNoColaborativo": 0,
    "hallazgos": 1,
    "existeDocumentacion": 0,
    "porcentajeSimilitud": 100,
    "porcentajeSimilitudInverso": 100,
    "nroDocumento": "",
    "nroIdTributaria": "",
    "institucion": "Ministerio de Seguridad de la Provincia de Buenos Aires -",
    "cargoFuncion": "Ministro de Seguridad",
    "relacion": null,
    "url": "http://info.pld.global/Base/Index?referencia=Ql9QRVBTX1NJX0FSfDE0MTI0MDc4",
    "domicilio": null,
    "paisSwift": "AR",
    "fechaNovedad": "2020-01-03T00:00:00",
    "evaluacion": 2,
    "esPEP": true,
    "esSancionado": false,
    "esSujetoObligado": false,
    "esTerrorista": false
  }
]
```



Deicsion

Terrorist List 

- Save the result
- Block the User - Yes
  - LOCKED_STATUS = Permanent Lock
  - Lock Reason = **TERRORIST**
  - Comment provide details
- Return genneral error by default - 'Sorry you can't register on our site'
- Should be allowed to check action log for details
- Should be allowed to check External Service Report for details

PEP List

- Save the result
- Block the User - No
- Should be allowed to check action log for details
- Should be allowed to check External Service Report for details

Congelados UIF List (Frozen list)

- Save the result
- Block the User - Yes
  - LOCKED_STATUS = Permanent Lock
  - Lock Reason = **AML/CFT**
  - Comment provide details
- Return genneral error by default - 'Sorry you can't register on our site'
- Should be allowed to check action log for details
- Should be allowed to check External Service Report for details

