## JSX

### JSX Creates Elements

When we created our `ReactElement` earlier, we used `React.createElement` like this:

```javascript
var boldElement = React.createElement('b', null, "Text (as a string)");
```

HTML works very well for specifying tag hierarchies. It would be nice to represent our React component tree using markup, much like we do for HTML.

This is the idea behind `JSX`.

When using JSX, creating the `ReactElement` objects are handled for us. Instead of calling `React.createElement` for each element, the equivalent structure in JSX is:

```react
var boldElement = <b>Text (as a string)</b>;
```

The JSX parser will read that string and call `React.createElement` *for us*.

JSX stands for **JavaScript Syntax Extension**, and it is a syntax React provides that looks a lot like HTML/XML. Rather than building our component trees using normal JavaScript directly, we **write our components almost as if we were writing HTML**.

JSX provides a syntax that is similar to HTML. However, in JSX we can create our own tags (which encapsulate functionality of other components).

Although it has a scary-sounding name, writing JSX is not much more difficult than writing HTML. For instance, here is a JSX component:

```react
const element = <div>Hello world</div>;	
```

One difference between React components and HTML tags is in the naming. HTML tags start with a lowercase letter, while **React components** start with an **uppercase**. For example:

```javascript
// html tag
const htmlElement = (<div>Hello world</div>);

// React component
const Message = props => (<div>{props.text}</div>)

// Use our React component with a `Message` tag
const reactComponent = (<Message text="Hello world" />);	
```

We **often surround JSX with parenthesis `()`**. Although this is not always technically required, it helps us set apart JSX from JavaScript.

Our browser doesn't know how to read JSX, so how is JSX possible?

**JSX is transformed into JavaScript by using a pre-processor build-tool** before we load it with the browser.

When we write JSX, we pass it through a "compiler" (sometimes we say the code is *transpiled*) that converts the JSX to JavaScript. The most common tool for this is a plugin to `babel`, which we'll cover later.

### JSX Attribute Expressions

In order to use a JavaScript expression in a component's attribute, we wrap it in curly braces `{}` instead of quotes `""`.

This example uses the [ternary operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator) on the `color` prop.

```react
const warningLevel = 'debug';
const component = (<Alert
                    color={warningLevel === 'debug' ? 'gray' : 'red'}
                    log={true} />)
```

If the `warningLevel` variable is set to `debug`, then the `color` prop will be `'gray'`, otherwise it will be `'red'`.

### JSX Conditional Child Expressions

Another common pattern is to use a boolean checking expression and then render another element conditionally.

For instance, if we're building a menu that shows options for an `admin` user, we might write:

We can also use the ternary operator to render one component or another.

For instance, if we want to show a `<UserMenu>` component for a logged in user and a `<LoginLink>` for an anonymous user, we can use this expression:

```react
const renderAdminMenu = function() {
  return (<MenuLink to="/users">User accounts</MenuLink>)
}
// ...
const userLevel = this.props.userLevel;
return (
  <ul>
    <li>Menu</li>
    {userLevel === 'admin' && renderAdminMenu()}
  </ul>
)
```



### JSX Boolean Attributes

In HTML, the presence of some attributes sets the attribute to true. For instance, a disabled `<input>` HTML element can be defined:

In React we need to set these as booleans. That is, we need to pass a `true` or `false` explicitly as an attribute:

If we ever need to *enable* the `input` above, then we set `formDisabled` to `false`.

### JSX Comments

We can define comments inside of JSX by using the curly braces (`{}`) with comment delimiters (`/* */`):

### JSX Spread Syntax

Sometimes when we have many props to pass to a component, it can be cumbersome to list each one individually. Thankfully, JSX has a shortcut syntax that makes this easier.

For instance, if we have a `props` object that has two keys:

We *could* pass each prop individually like this:

But by using the JSX *spread syntax* we can do it like this instead:

### JSX Gotchas

Although JSX mimics HTML, there are a few important differences to pay attention to.

Here's a few things to keep in mind:

#### JSX Gotcha: `class` and `className`

When we want to set the CSS class of an HTML element, we normally use the `class` attribute in the tag:

Since JSX is so closely tied to JavaScript, we cannot use identifiers that JavaScript uses in our tag attributes. Attributes such as `for` and `class` conflict with the JavaScript keywords `for` and `class`.

Instead of using `class` to identify a class, JSX uses `className`:

The `className` attributes works similarly to the `class` attribute in HTML. It expects to receive a string that identifies the class (or classes) associated with a CSS class.

To pass multiple classes in JSX, we can join an array to convert it to a string:

#### Tip: Managing `className` with `classnames`

The `classnames` [npm package](https://www.npmjs.com/package/classnames) is a great extension that we use to help manage css classes. It can take a list of strings or objects and allows us to conditionally apply classes to an element.

The `classnames` package takes the arguments, converts them to an object and conditionally applies a CSS class if the value is truthy.

The [package readme](https://github.com/JedWatson/classnames/blob/master/README.md) provides alternate examples for more complex environments.

#### JSX Gotcha: `for` and `htmlFor`

For the same reason we cannot use the `class` attribute, we cannot apply the `for` attribute to a `<label>` element. Instead, we must use the attribute `htmlFor`. The property is a pass-through property in that it applies the attribute as `for`:

#### JSX Gotcha: HTML Entities and Emoji

*Entities* are reserved characters in HTML which include characters such as less-than `<`, greater-than `>`, the copyright symbol, etc. In order to display entities, we can just place the entity code in literal text.

In order to display entities in dynamic data, we need to surround them in a string inside of curly braces (`{}`). Using unicode directly in JS works as expected. Just as we can send our JS to the browser as UTF-8 text directly. Our browser knows how to display UTF-8 code natively.

Alternatively, instead of using the entity character code, we can use unicode version instead.

Emoji are just unicode character sequences, so we can add emoji the same way:

![Everyone needs more dolphins](https://d2uusema5elisf.cloudfront.net/books/fullstack-react/images/components/jsx/jsx-emoji-dolphins.png)

#### JSX Gotcha: `data-anything`

If we want to apply our own attributes that the HTML spec does not cover, we have to prefix the attribute key with the string `data-`.

This requirement *only* applies to DOM components that are native to HTML and does not mean custom components cannot accept arbitrary keys as props. That is, we can accept *any* attribute on a custom component:

There are a standard set of [web accessibility](https://www.w3.org/WAI/intro/aria) [attributes](https://www.w3.org/TR/wai-aria/) and its a good idea to use them because there are many people who will have a hard time using our site without them. We can use any of these attributes on an element with the key prepended with the string `aria-`. For instance, to set the `hidden` attribute:

### JSX Summary

JSX isn't magic. The key thing to keep in mind is that JSX is syntactic sugar to call `React.createElement`.

JSX is going to parse the tags we write and then create JavaScript objects. JSX is a convenience syntax to help build the component tree.

As we saw earlier, when we use JSX tags in our code, it gets converted to a `ReactElement`:

We can pass that `ReactElement` to `ReactDOM.render` and see our code rendered on the page.

There's one problem though: `ReactElement` is stateless and immutable. If we want to add interactivity (with state) into our app, we need another piece of the puzzle: `ReactComponent`.

In the next chapter, we'll talk about `ReactComponent`s in depth.