## How does Hook API work?

TBD

## Places that Hook should not be used

- Hook can not be used with 
  - **if...else** 
  -  **loop**

<iframe width="900" height="506" src="https://www.youtube.com/embed/KJP1E-Y-xyo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

