# Component 

## Props

A child component does not own its **props**. Parent components own the **props** of child components

## States

**State is owned by the component** and private to the component

When a state or a props of a component update, the component will re-render itself

#### Example

##### Loading

 ```javascript
if (loading) {
      return <div className="list-items">loading</div>;
}
 ```

##### Empty

```javascript
if (items.length === 0) {
    return <div className="list-items">empty</div>;
  }
```

##### Tables

```javascript
return (
    <div className="list-items">
      {items.map(items => (
        <Item key={item.id} item={item} {...events} />
      ))}
    </div>
  );
```

## Method binding

**any time we define our own custom component methods, we have to manually bind `this` to the component ourselves**. There's a pattern that we use to do so.

