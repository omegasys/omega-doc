# Args

A story is a component with a set of arguments (props, slots, inputs, etc). “Args” are Storybook’s mechanism for defining those arguments as a first class entity that’s machine readable. This allows Storybook and its addons to **live edit** components. You *do not* need to change your underlying component code to use args.

When an arg’s value is changed, the component re-renders, allowing you to interact with components in Storybook’s UI via addons that affect args.

Learn how and why to write stories in [the introduction](https://storybook.js.org/docs/react/writing-stories/introduction#using-args). For details on how args work, read on.

## Args object

The args object can be defined at the story and component level (see below). It is an object with string keys, where values can have any type that is allowed to be passed into a component in your framework.

## Story args

To define the args of a single story, use the `args` CSF story key:


```js
// Button.stories.js

import React from 'react';

const Template = (args) => <Button {...args} />;

// Each story then reuses that template
export const Primary = Template.bind({});

Primary.args = {
  primary: true,
  label: 'Primary',
};
```

Copy

These args will only apply to the story for which they are attached, although you can [reuse](https://storybook.js.org/docs/react/workflows/build-pages-with-storybook#args-composition-for-presentational-screens) them via JavaScript object reuse:

```js
// Button.stories.js

export const PrimaryLongName = Template.bind({});

PrimaryLongName.args = {
  ...Primary.args,
  label: 'Primary with a really long name',
}
```

Copy

In the above example, we use the [object spread](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax) feature of ES 2015.

## Component args

You can also define args at the component level; such args will apply to all stories of the component unless they are overwritten. To do so, use the `args` key of the `default` CSF export:

- 
- 

```js
// Button.stories.js

import React from 'react';
import Button from './Button';

export default {
  title: "Button",
  component: Button,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
  args: {
    // Now all Button stories will be primary.
    primary: true,
  },
};
```

Copy

## Args composition

You can separate the arguments to a story to compose in other stories. Here’s how args can be used in multiple stories for the same component.

```js
// Button.stories.js

const Primary = ButtonStory.bind({});
Primary.args = {
  primary: true,
  label: 'Button',
}

const Secondary = ButtonStory.bind({});
Secondary.args = {
  ...Primary.args,
  primary: false,
}
```

Copy

Note that if you are doing the above often, you may want to consider using [component-level args](https://storybook.js.org/docs/react/writing-stories/args#component-args).

Args are useful when writing stories for composite components that are assembled from other components. Composite components often pass their arguments unchanged to their child components, and similarly their stories can be compositions of their child components stories. With args, you can directly compose the arguments:

- 
- 

```js
// Page.stories.js

import React from 'react';
import { Page } from './Page';
import * as HeaderStories from './Header.stories';

export default {
  title: 'Page',
  component: Page,
};

const Template = (args) => <Page {...args} />;

export const LoggedIn = Template.bind({});
LoggedIn.args = {
  ...HeaderStories.LoggedIn.args,
};
```

Copy