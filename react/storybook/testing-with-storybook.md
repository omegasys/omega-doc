Testing with Storybook

As you add more stories, manual testing becomes infeasible. We recommend automating testing to catch bugs and regressions. A complete Storybook testing strategy combines the following techniques to balance coverage, accuracy, and maintainability:

- <u>Manual tests</u> rely on developers to manually look at a component to verify it for correctness. They help us sanity check a component’s appearance as we build.

- [Unit tests](https://storybook.js.org/docs/react/workflows/unit-testing) verify that the output of a component remains the same given a fixed input. They’re great for testing the functional qualities of a component.

  - There are [many tools](https://github.com/mojoaxel/awesome-regression-testing) for visual testing. Storybook uses [Chromatic](https://www.chromatic.com/), a visual testing service made by Storybook maintainers to run tests in the cloud across browsers. Capture screenshots of every story and compare them against known baselines. They’re great for catching UI appearance bugs.   
  ![Visually testing a component in Storybook](https://storybook.js.org/b2d5cc75d84f4519e390a495ebc0b949/component-visual-testing.gif)

- [Interaction tests](https://storybook.js.org/docs/react/workflows/interaction-testing) render a story and then interact with it in the browser, asserting things about the way it renders and changes.

- [Snapshot tests](https://storybook.js.org/docs/react/workflows/snapshot-testing) compare the rendered markup of every story against known baselines. This catches markup changes that cause rendering errors and warnings.

