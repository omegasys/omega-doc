## Configure story loading

### main.js

By default, Storybook will load stories from your project based on a glob (pattern matching string) in `.storybook/main.js` that matches all files in your project with extension `.stories.js`. The intention is you colocate a story file with the component it documents.

```
└── components
        ├── Button.js
        └── Button.stories.js
```

For example if you wanted to pull both `.md` and `.js` files from the `my-project/src/components` directory, you could write:

```js
// .storybook/main.js
module.exports = {
  stories: ['../my-project/src/components/*.@(js|md)'],
};
```

## Configure story rendering

To control the way stories are rendered and add global [decorators](https://storybook.js.org/docs/react/writing-stories/decorators#global-decorators) and [parameters](https://storybook.js.org/docs/react/writing-stories/parameters#global-parameters), create a `.storybook/preview.js` file. This is loaded in the Canvas tab, the “preview” iframe that renders your components in isolation. Use `preview.js` for global code (such as [CSS imports](https://storybook.js.org/docs/react/get-started/setup#render-component-styles) or JavaScript mocks) that applies to all stories.

The `preview.js` file can be an ES module and export the following keys:

- `decorators` - an array of global [decorators](https://storybook.js.org/docs/react/writing-stories/decorators#global-decorators)
- `parameters` - an object of global [parameters](https://storybook.js.org/docs/react/writing-stories/parameters#global-parameters)
- `globalTypes` - definition of [globalTypes](https://storybook.js.org/docs/react/essentials/toolbars-and-globals#global-types-and-the-toolbar-annotation)

## Configure story theme context

If a particular story has a problem rendering, often it means your component expects a certain environment is available to the component.

A common frontend pattern is for components to assume that they render in a certain “context” with parent components higher up the rendering hierarchy (for instance theme providers)

Use [decorators](https://storybook.js.org/docs/react/writing-stories/decorators) to “wrap” every story in the necessary context providers. [`.storybook/preview.js`](https://storybook.js.org/docs/react/configure/overview#configure-story-rendering) allows you to customize how components render in Canvas, the preview iframe. In this decorator example, we wrap every component rendered in Storybook with `ThemeProvider`.

```js
// .storybook/preview.js

import React from "react";
import { ThemeProvider } from 'styled-components';

export const decorators = [
  (Story) => (
    <ThemeProvider theme="default">
      <Story />
    </ThemeProvider>
  ),
];
```