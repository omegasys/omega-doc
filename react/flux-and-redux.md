## Flux

As apps grow in size and complexity, **managing state** inside of React components (or the *component-state paradigm*) can become cumbersome.

A common pain point is the **tight coupling between user interactions and state changes**. For complex web applications, oftentimes a single user interaction can affect many different, discrete parts of the state.

For example, consider an app for managing email. Clicking an email must:

1. Replace the "inbox view" (the list of emails) with the "email view" (the email the user clicked)
2. Mark the email as read locally
3. Reduce the total unread counter locally
4. Change the URL of the browser
5. Send a web request to mark the email as read on the server

### Flux is a design pattern

Flux is a design pattern. The predecessor to Flux at Facebook was another design pattern, [Model-View-Controller](https://en.wikipedia.org/wiki/Model–view–controller) (MVC). MVC is a popular design pattern for both desktop and web applications.

In MVC, user interactions with the View trigger logic in the Controller. The Controller instructs the Model how to update itself. After the Model updates, the View re-renders.

While React does not have three discrete "actors" like a traditional MVC implementation, it suffers from the same coupling between user interactions and state changes.

### Flux overview

The Flux design pattern is made up of four parts, organized as a one-way data pipeline:

![Flux diagram](https://d2uusema5elisf.cloudfront.net/books/fullstack-react/images/5-intro-to-flux/flux-diagram.png)

- The **view** dispatches **actions** that describe what happened.
- The **store** receives these actions and determines what state changes should occur. 
- After the state updates, the new state is pushed to the View.

Returning to our email example, in Flux, we no longer have a single function handling email clicks that describes all of the state changes. Instead, React notifies the store (through an action) that the user clicked on an email. As we'll see over the next few chapters, we can organize the store such that each discrete part of the state has its own logic for handling updates.

### Flux benefits

Flux also provides the following benefits:

#### **Breaking up state management logic**

As parts of the state tree become interdependent, most of an app's state usually gets rolled up to a top-level component. Flux relieves the top-level component of state management responsibility and allows you to break up state management into isolated, smaller, and testable parts.

#### **React components are simpler**

Certain component-managed state is fine, like activating certain buttons on mouse hover. But by managing all other state externally, React components become simple HTML rendering functions. This makes them smaller, easier to understand, and more composable.

#### **Mis-match between the state tree and the DOM tree**

Oftentimes, we want to store our state with a different representation than how we want to display it. For example, we might want to have our app store a timestamp for a message (`createdAt`) but in the view we want to display a human-friendly representation, like "23 minutes ago." Instead of having components hold all this computational logic for *derived data*, we'll see how Flux enables us to perform these computations *before* providing state to React components.

We'll reflect on these benefits as we dig deep into the design of a complex application in the next chapter. Before that, we'll implement the Flux design pattern in a basic application so that we can review Flux's fundamentals.

## Redux

**Redux** is a state management paradigm based on Facebook's Flux architecture. Redux provides a structure for large state trees and allows you to **decouple user interaction** in your app **from state changes**.

### Redux's key ideas

Throughout this chapter, we'll become familiar with each of Redux's key ideas. Those ideas are:

![image-20201211054728742](flux-and-redux-overview.png)

- All of your application's data is in a single data structure called the **state** which is held in the **store**
- Your app reads the **state** from this **store**
- The **state** is never mutated directly outside the **store**
- The **views** emit **actions** that describe what happened
- A **new state** is created by combining the **old state** and the **action** by a function called the **reducer**
![image-20201211113014598](flux-and-redux-#2store.png)

### Actions

Actiosn in Redux are objects. Actions always have a **type** property.

Our increment actions will look like this:

``` javascript
{
  type: 'INCREMENT',
}
```



And decrement actions like this:

``` javascript
{
  type: 'DECREMENT',
}
```

We can envision what a simple interface for this counter app might look like:

![An example counter interface](https://d2uusema5elisf.cloudfront.net/books/fullstack-react/images/5-intro-to-flux/counter-1.png)

When the user clicks the "+" icon, the view would dispatch the increment action to the store. When the user clicks the "-" icon, the view would dispatch the decrement action to the store.

> The image of the interface for the counter app is provided as just an example of what the view *might* look like. We will not be implementing a view layer for this app.