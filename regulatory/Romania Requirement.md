## 1. Game type report

> Remote Gaming Systems must be able to provide through the ONJN terminal a Game Type Report (or a similarly named report) upon request for the specified period and at least for the month before the current date, the year before the current date and the total operation prior to the current date for each individual game (eg payable). The report must contain at least the following information:
> a)   Selected range;
> b)   Type of game;

What is Type of game? Category? Classification? 

> c)   Total number of participants in the game;
> d)   Total bet, including bonus amounts;
> e)   Total amount won, including bonus amounts;
>  f)    The total amount added to the progressive jackpots

How do we obtain the progressive jackpot information? more info needed

>  g)   Total amount refunded;

What is refund in context? Rollback of GAME_BET?

> h)   The income of the organizer in accordance with the provisions of art. 1 ^ 1 of GEO no. 77/2009;

Income? GAME_WIN? Who is the organizer? 

> i)    Total funds for incomplete games.

That means sum of GAME_BET whose game round is still open? 



## 2. Game Profit Report

> Remote Gaming Systems must be able to provide through the ONJN terminal a Gaming Profit Report (or a similarly named report) upon request for the specified reporting period and at least for the month prior to the current date, the year prior to the current date and operation totals prior to the current date. The report must contain at least the following information:
> a)   The total amounts deposited in the player's accounts;
> b)   The total amounts withdrawn from the player's accounts;
> c)   All amounts currently held in the player's accounts excluding bonuses.

Balance Real + Balance RB?

> d)   Operator income excluding bonuses

 GGR (R + RB)?

## 3. Jackpot Configuration Report

> Remote Gaming Systems must be able to provide through the ONJN terminal a Jackpot Configuration Report (or a similarly named report) upon request for each progressive jackpot on the site for the specified reporting period and at least for the month before the current date, the year before the current date and the total operation before the current date. The report mustcontains at least the following information:
> a)   The name of the progressive jackpot;
> b)  Progressive jackpot start date and time;
> c)   Configuration parameters (daily amount, prize percentage, etc.) of all main and secondary jackpots (including any redirected amounts);

Source of the info? What are the providers? Jackpot Configuration would be sync to CORE? Not available today.

> d)  The unique payroll item for each existing game;

Payroll item? Jackpot Payout?

> e)   The total amount of bets eligible for the progressive jackpot (s);
> f)   The total amount of progressive jackpots won;
> g)  All jackpot contributions won;

Not all providers share the same type of info. Consistency? 

> h)  Starting the jackpot or other non-funded values

What is non-funded values?

> i)    The current amount of each jackpot prize offered in that jackpot;

Example would be good to help understand this.

> j)    Current value of redirected jackpot contributions;

Redirected jackpot CB?

> k)  Jackpot completion date and time;
> l)    Progressive jackpot limit value, if any;
> m)  The amount that exceeds the limit, if any.

What type of limit? I think if we use one game provider progressive jackpot as example it will help



## 4. Jackpot Earnings Report

> Remote Gaming Systems must be able to provide through the ONJN terminal a Jackpot Affair Winning Report (or a similarly named report) upon request for each progressive jackpot on the site for the specified reporting period and at least for the month before the current date, the year before the current date and the total operation before the current date. The report must contain at least the following information:

> a)   The name of the jackpot;
> b)   The unique element of the game's payroll identification;

Payroll? 

> c)   Unique game session ID;
> d)   IDplayer's unique;
> e)   Cycle (round) IDthe game;
> f)    Jackpot prize date and time;
> g)   Maximum jackpot level;
> h)   Jackpot amount;
> i)    User ID and name of the employee processing the earnings, if any; and
> j)    User ID and name of the supervisor confirming the win, if applicable;

Haven't seen the staff (employee) process the jackpot. Use case would help.



## 5. Significant Events Report

> Remote Gaming Systems must be able to provide through the ONJN terminal a Significant Event Report (or similarly named report) upon request for the specified reporting period and at least for the month prior to the current date, year prior to the current date and operation totals prior to the current date. The report must contain at least the following information:

> a)  Failed attempts to connect to the game platform;

What is the game platform in context? OMEGA platform? Game Provider?

> b)  The periods in which the Remote Gaming System was not accessible or could not process the requests / transactions of any kind of the participants in the game;

System downtime? i.e. Request per miniute = 0

> c)   Statistical reports, for selectable time periods, on the operation of the central computer system, the firewall, the mirror server and the terminal located at ONJN;

Infrastructure server status history? Monitoring tool?

> d)         Earnings in excess of EUR 15,000;

GAME WIN for a given player? or for all players within a period?

> e)          Financial transactions (simple and time-bound) that exceed value of 15,000 EURO;

Absolute values of each transactions? BET and WIN can offset each other. Do we care about BET, or just WIN / Product Bonus?

> f)          Number of adjustments of financial transactions operated by the organizer in the players' accounts;

What is adjustments in context? Staff adjustment? 

> g)         Number of bans grouped by reason for exclusion;

Player lock reason report?

> h)   Any other activity which required the intervention of an employee and which took place outside the normal scope of operation of the system;

Need example and use cases. Player Review / Intervention Log?

> i)         Other significant or unusual events.

Hard to measure unusual -> scope needed. 



## 6. Change Notification Report

> Remote Gaming Systems must be able to provide via the ONJN terminal a Change Notification Report (or a similarly named report) for any changes to the system and game configuration or parameters upon request for the period. specified in the report and at least for the month before the current date, the year before the current date and the total operation before the current date. Licensed companies must provide a comparison report between previous and new game settings. The report must contain at least the following information:

> a)       The way of auditing / verifying the information changed / modified by the administration accounts;
> b)             Date / time changes on the main server;
> c)              Parameter changes;

- User Action Log with StaffID?
- Also Bonus Plan, Registry etc need to be fully audited. (Expensive!)

## 7. Exclusion Report

> Remote Gaming Systems must be able to provide through the ONJN terminal an Exclusion Report (or a similarly named report) for all excluded or self-excluded players upon request for the specified reporting period and at least for the month prior to the current date, year prior to the current date and the total operation prior to the current date. The report must contain at least the following information:

> a)   IDplayer's unique;
> b)   Type of exclusion (Permanent, self-exclusion, etc.);
> c)   Date of start of exclusion;
> d)   Date of termination of exclusion, if any;
> e)   Reason for exclusion;
> f)    Whenever the player was excluded (on the date of the report).

We might already have some player lock report that does this. Player lock reason?



## 8. Account Balance Adjustment Report

> Remote Gaming Systems must be able to provide through the ONJN terminal a Remote Gaming Account Balance Adjustment Report (or a similarly named report) upon request for each daily adjustment of each ID of authorized players and at least for the month before the current date, the year before the current date and the total operation before the current date. The report must contain at least the following information:

> a)   The name and account number of the authorized player;
> b)   Date and time of account balance adjustment;
> c)   Unique transaction number;
> d)   The amount by which the account balance was adjusted;
> e)   Account balance before adjustment;
> f)    Account balance after adjustment;
> g)   Account adjustment type; and
> j) Reason / description of account balance adjustment.

Account Tran shows for each transaction that affects player's balance. What is Adjustment in context? translation issue? Accounting > Player Transaction Report



## 9. Promotional Elements Summary Report.

> Remote Gaming Systems must be able to provide, through the ONJN terminal, a Centralized Report of Promotional Items (or a similarly named report) upon request for each promotions and / or loyalty bonuses and at least for the month prior to the current date, year prior to the current date and the total operation prior to the current date. The report must contain at least the following information:

Bonus Plan Summary Report

> a)      Promotion ID and / or bonus;
> b)      Type of promotion and / or bonus;
> c)      Start date promotion and / or bonus;
> d)      End of promotion and / or bonus date;
> e)      Promotion and / or bonus status (eg ongoing, completed, etc.)
> f)       The total balance allocated to the promotion and / or bonus;
> g)      Total amount of prizes for promotion and / or bonus;

need clarify **amount of prizes** 

> h)      The total amount used for promotion and / or bonus;
> i)       Total amount of adjustment for promotion and / or bonus;

Adjustment? Change? 

> j)       Final balance of the promotion and / or bonus.



## 10. Software change management report

> Remote Gaming Systems must be able to provide through the ONJN terminal a Software Change Management Report (or a similarly named report) upon request for all software changes made to the organizer's computer system and at least for the month prior to the current date. , the year before the current date and the total operation before the current date.

Software Change Management Report -> Release Notes?

> Software changes related to changes to the random number generator, game account registration / modification, game logic and payment methods require recertification of the game software and will be implemented after the approval of the ONJN Committee.

- game account registration / modification -> Signup?
- game logic and payment methods require -> Game / Payment Integration

The above needs to be approved?

>  The report must contain at least the following information:
> a)                 Previous version;
> b)                Current version;
> c)                 Date of software change;
> d)                Description of software change;
> e)                 Software change category;
> f)                 The reason for the software change;

Release notes



## 11. Mirror server report

> Summary log of reports submitted to this server, upon request for the specified reporting period and at least for the month before the current date, the year before the current date and the total operation prior to the current date, according to Annex 2.

Server operation access log? Infrastructure monitoring 

## 12. Firewall report

> Summary log of reports submitted to this server, upon request for the specified reporting period and at least for the month before the current date, the year before the current date and the total operation before the current date, according to Annex 3.

Infrastructure firewall access / change log



## General Reporting Requirements

>  **1.** The central computer system authorized for the Remote Gambling Organizer will generate, at the end of the game session, using a timestamp, a report with the requested information. The report will be transmitted in real time to the mirrored server.

At the end of game session? What is the game session (define game session starts & end)
Real time? **Performance** concern?

> **2.** The central computer system must have a mechanism for exporting or transmitting to ONJN the data reported in the mirror server, in a **CSV** format, for the purpose of analyzing and inspecting / verifying the data.

>  **3.**  The system must be able to keep the data in the report for a period of 5 years from the date of their collection and processing.

>  **4.** The organizer will ensure the identity, authenticity, confidentiality and non-repudiation of the data in the mirror server.

need clarfication on mirror server

> **5.**  The organizer will provide the authority with a way to verify the electronic signature and the decryption key.

Data encryption? At DB server level?

>  **6.** The organizer will provide the authority with the way in which the information contained on this server is complete and can be found in the central computer system.

> **7.**  The report will be signed with the electronic signature according to the legal provisions in force (Law no. 455/2001 on the electronic signature, with subsequent amendments and completions).

> **8.** The generated report will be temporarily marked according to the legal provisions in force (Law no. 451/2004 on the time stamp).

What does that imply?

>  **9.**  The mirror server must be able to generate centralized reports for selectable time periods.

>  **10.** Reports sent to the mirrored server will include at least the following game session data:
> **Game session** for betting games 
>
> - it starts when a player registers as a participant in the game 
> - and ends when the **winner of the game has been determined**.
> -  For other types of gambling, the session begins when the player logs in to the game account (login) and ends when he logs out of the game account (logout).

The game session definition changes based on the type of the game? Some of them with login session, some of them with actual game session

> **11.** The report of the game session must contain at least the following information:
>
> a)   The type of game;
> b)   Unique game session ID;
> c)   IDplayer's unique;
> d)   Time to start the session;
> e)   Time to end the session;
> f)    Relevant geolocation information;

Geolocation at game session level?

> g)   The amount wagered during the session;
> h)   The amount earned during the session;
> i)    Amount lost during the session;
> j)    Organizer's income;
> k)   Promotions or bonuses received during the session;
> l)    Promotions or bonuses wagered during the session;
> m) Funds deposited in the player's authorized account during the session;
> n)   Funds withdrawn from the player's authorized account during the session;
> o)   The reason for the suspension of the session;

Can a session be suspended? How?

> p)   The balance of the game account at the beginning of the session;
> q)   The balance of the game account at the end of the session;

We didnt capture this info. That means we need to calculated based on the first transaction of the session from account_tran, or we capture this when the session start. TODO

> r)    Funds remaining in incomplete games.

Incomplete means open session (end game not been called by provider), but the fund remaining means sum of bets?



## GENERAL REPORTING REQUIREMENTS

> **1.** The central computer system authorized for the Remote Gambling Organizer will generate a report with the requested information when performing a transaction (operation). The report will be transmitted in real time to the firewall.

> **2.**   The transaction means any registration in the central computer system of the organizer which refers to:
>
> - personal data of the game participant;

Profile update?

> - its financial operations;
> - each operation in a game session (includes each hand played, each bet placed) and
> - issues related to player protection (self-exclusion, exclusion, etc.) related to Romanian players or players using Romanian IP.

RG?

> **3.** The organizer will ensure the identity, authenticity, confidentiality and non-repudiation of data on the firewall.
> **4.**  The organizer will provide the authority with the method of verifying the electronic signature and the decryption key.
> **5.** The organizer will make available to the authority the way in which it can be verified that the information contained on this server is complete and can be found in the central computer system. This, as well as the established reports on the operation of the firewall, must be accessible from the terminal located at the National Gambling Office.
> **6.** The report will be signed with electronic signature, according to the legal provisions in force (Law no. 455/2001 on electronic signature, with subsequent amendments and completions).
> **7.** The generated report will be temporarily marked, according to the legal provisions in force (Law no. 451/2004 on the time stamp).
> **8.**  Once the data has been transmitted to the firewall, the remote gambling organizer will no longer be able to modify the transmitted data.

That means the transmitted data needs to be read only. how to ensure that? Who controls the file access mode?



| Registration type has          | Field | Field sublevel 1 | Field sublevel 2 | Generation trigger               | Date type | Ocur Min | Ocur Max | Field description | Example |
| ------------------------------ | ----- | ---------------- | ---------------- | -------------------------------- | --------- | -------- | -------- | ----------------- | ------- |
| Common ground for all  records |       |                  |                  | It depends on the type of record |           |          |          |                   |         |

| countries |                               |      |      |      |                     |      |      |                                                              |                                             |
| --------- | ----------------------------- | ---- | ---- | ---- | ------------------- | ---- | ---- | ------------------------------------------------------------ | ------------------------------------------- |
|           | VersionForm at                |      |      |      | Positive integer    | 1    | 1    | Version of the data model used.  This allows  easy implementation of format  evolutions and coexistence of different versions of the model in the data  warehouse | 1                                           |
|           | IDRegistration                |      |      |      | Character string is | 1    | 1    | This field uniquely identifies  Register.  Two  recordings in the safe cannot be  shared  never the same registration ID.  The operator will use a UUID for  this field. | 254a26b5-  5194-465d-  b1c5- 063231490d  60 |
|           | Operator ID                   |      |      |      | Character string is | 1    | 1    | This is  the ID of the generating operator  Register. If a provider generates  the registration on behalf of an operator, the operator ID is used. This ID  is provided by the authority that  licensed to this operator. | French_Ope rator (French  operator)         |
|           | ID  Generator in registration |      |      |      | Character string is | 0    | 1    | If the operator generated the  record, this field is missing. Otherwise, this is the ID of the provider that  generates the record. The value  of this ID is chosen by the vendor | Network_O operator (network  operator)      |
|           | Date of extraction            |      |      |      | Date Time           | 1    | 1    | This is the time it was created  registration (ie the time at  which it was extracted from the computer system). This is NOT the time the  record is sent or recorded in the safe. UTC will be used to prevent DST and  other issues | 2011-10-  04T19:  53: 51  .346Z             |

> Correction Mark: This optional field is only used  when  the current record replaces or  cancels an incorrectly submitted  previous record. In this case, the correct new record uses  sameIDRegistration and this field is  set to  "true". This is the  only exception when the Registration ID can be reused. The file can still be  uniquely identified by the registration and extraction date pair.  To cancel a registration, a  registration is sent  "Blank", containing  only the items in this section (common basis for all records)

How can a registration be cancelled?

|                | Correction mark |      |      |                                                              | Bool                | 0    | 1    | This optional field is only used  when  the current record replaces or  cancels an incorrectly submitted  previous record. In this case, the correct new record uses  sameIDRegistration and this field is  set to  "true". This is the  only exception when the Registration ID can be reused. The file can still be  uniquely identified by the registration and extraction date pair.  To cancel a registration, a  registration is sent  "Blank", containing  only the items in this section (common basis for all records) | fake               |
| -------------- | --------------- | ---- | ---- | ------------------------------------------------------------ | ------------------- | ---- | ---- | ------------------------------------------------------------ | ------------------ |
| Player details |                 |      |      | At the wrong registration the  player ui and when  modify information i |                     |      |      |                                                              |                    |
|                | First name      |      |      | Register re                                                  | Character string is | 1    | 1    | First name of the player being  registered. If the player's first name is compounded, the first name will be  separated by "-" (hyphen). If  the player has a second or more  first name, all must be indicated and separated by ""  (unique space) | Ion Marian Popescu |
|                | Name            |      |      | Register re                                                  | Character string is | 1    | 1    | This field contains the player's  last name. If the player has more than one last name, all must be indicated  and separated by ""  (unique space).  The last name is   the one currently used in  official identity documents and may differ from the name of the date  birth. | Ion Barbu          |

|      |                 |      |      |                             |                     |      |      |                                                              |                         |
| ---- | --------------- | ---- | ---- | --------------------------- | ------------------- | ---- | ---- | ------------------------------------------------------------ | ----------------------- |
|      | Date of birth   |      |      | Register re                 | Date                | 1    | 1    | The date of the player's birth                               | 1895-03-18              |
|      | CNP             |      |      | Register re                 | Number              | 1    | 1    | This field contains the Personal  Numeric Code for Romanian citizens. | 1900806182  888         |
|      | Tax residency   |      |      | Register re                 | Character string is | 1    | 1    | Tax residence of the player                                  | Romania                 |
|      | Street / number |      |      | Register and change address | Character string is | 1    | 1    | This is the street and the  building number of the player's main residence, as it appears in his identity  documents. | Narciselor Street no. 3 |
|      | Postal  code    |      |      | Register and change address | Number              | 1    | 1    | This  is the code  of the principal domicile of  the player, as it appears in his  identity documents.  The code may contain letters and  other characters (because the player  may live in another  country) | 96541                   |
|      | local           |      |      | Register and change address | Character string is | 1    | 1    | This is the location of the  player's main residence, as it appears in his identity documents. | Bucharest               |
|      | The country     |      |      | Register and change address | Character string is | 0    | 1    | This is the ISO 3166-1 code  (ALPHA-2) of  country of main residence of  the player, as it appears in his  identity documents.  This field is required if  the player does not live in the  country | EN                      |