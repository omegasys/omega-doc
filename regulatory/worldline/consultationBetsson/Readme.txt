﻿Consultation/validation of traces
2023/01/18

1. "Consultation" process: 

   In <INSTALLATION_DIR>/consultation directory
   a) Setup propertiesExtraction.properties
         replace <INSTALLATION_DIR> tag, and update other information
   b) execute runClientExtraction.cmd propertiesExtraction.properties
   c) results are in <INSTALLATION_DIR>/result/logExtraction.log (see response.file property)
   
2. "Download"

   Please visit URL provided in the result log file (see step 1c); please install your client certificate (with private key) in your browser/tool
   Unzip downloaded result file to <INSTALLATION_DIR>/validation/archives

3. "Validation" process

   In <INSTALLATION_DIR>/validation directory
   a) Setup etc/application.properties
   b) execute runValidationTool-v3.cmd application.properties
       if you have the following error "java.security.InvalidKeyException: Illegal key size", please copy lib/US_export_policy.jar and local_policy.jar in JAVA_HOME/jre/lib/security
   c) in case of success, results are stored in <INSTALLATION_DIR>/validation/EXTRACT