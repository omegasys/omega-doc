REM @ECHO OFF

IF NOT EXIST "%1" GOTO USAGE

SETLOCAL enabledelayedexpansion
FOR /f %%i IN ('TYPE %1 ^| FIND /V "#"') DO SET %%i

IF NOT "%~2" == "" (
SET method=%2
)

IF NOT "%~3" == "" (
SET report_id=%3
)

SET propertiesFile=%~f1
SET COMMAND=%java% -jar %extracter_path% -propertiesFile "%propertiesFile%" -method "%method%" -opId "%operator_id%" -vaultId "%vault_id%" -reportId "%report_id%" -constraintDate "%constraint_date%" -argConstraint "%arg_constraint%" -beginConstraint "%begin_constraint%" -endConstraint "%end_constraint%"
REM SET COMMAND=%java% -Dhttp.proxyHost="proxy-internet.localnet" -Dhttp.proxyPort=3128 -jar %extracter_path% -Dhttp.proxyHost="" -Dhttp.proxyPort=3128 -propertiesFile "%propertiesFile%" -method "%method%" -opId "%operator_id%" -vaultId "%vault_id%" -reportId "%report_id%" -constraintDate "%constraint_date%" -argConstraint "%arg_constraint%" -beginConstraint "%begin_constraint%" -endConstraint "%end_constraint%"
ECHO %COMMAND%
%COMMAND%

GOTO END

REM -----------------------------------------------------------------------------------------------
:USAGE
REM -----------------------------------------------------------------------------------------------
ECHO "Usage : runClientExtraction.cmd <propertiesExtration.properties>"

:END
ECHO "End"