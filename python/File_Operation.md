## File IO 

### Mode

| Character | Meaning                                                      |
| :-------- | :----------------------------------------------------------- |
| `'r'`     | open for reading (default)                                   |
| `'w'`     | open for writing, truncating the file first                  |
| `'x'`     | open for exclusive creation, failing if the file already exists |
| `'a'`     | open for writing, appending to the end of the file if it exists |
| `'b'`     | binary mode                                                  |
| `'t'`     | text mode (default)                                          |
| `'+'`     | open for updating (reading and writing)                      |

The default mode is `'r'` (open for reading text, synonym of `'rt'`). Modes `'w+'` and `'w+b'` open and truncate the file. Modes `'r+'` and `'r+b'` open the file with no truncation.

### Sample Code

The code below would open up matched .txt files, search keyword defined in the file and output the matched files path to the target file

```python
import os

# Set the path of the folder，obtain all the file name under the path
path = './WorkDir/'
files_list = os.listdir(path)

# Set the keyword to search
key_word = input("Please enter the keyword：")

# Open the file in order to append searched result later
result_file = open('./demo_result.txt', 'a', encoding='utf-8')

for file_name in files_list:
    if '.txt' in file_name:
        # Print the matched .txt file
        print("File Found ：" + file_name)

        # create full qualified file name based on path and actual file name
        target_file = path + file_name

        # Open file, read the content and close the file
        file = open(target_file, 'r', encoding='utf-8')
        content = file.read()
        file.close()

        # Check if keyword exists in the content
        if key_word in content:
            # Print found keyward
            print("Great，File**{}**contains the keyword：{}".format(target_file, key_word))

            # Write the matched full qualified file path to the target file
            result_file.write(target_file + '\n')

# close target file
result_file.close()


```

![img](https://res.pandateacher.com/UFOTH9D51596180612012.png)