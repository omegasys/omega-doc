

## Python Overview

3 Major areas, see below

### Automation

Avoid repititive work

### Crawler

Gather public open data

### Data Analysis 

analyse data for decision making

## Module and Library

### Library

If python is like a cellphone, the the library is like the app in the phone

<img src="https://res.pandateacher.com/XV2STIB61594815107680.png" alt="img" style="zoom:50%;" />

#### Standard Lib

Libraries provided out of the box by Python without manual installation

#### 3rd Party Lib

Libraries provided by other developers or organizations, which requires manual installation. 

<img src="https://res.pandateacher.com/HV9IUF3S1594813881987.png" alt="img" style="zoom:50%;" />

### Module

A module is a python file that can exists independently. Library is consisted of multiple modules.

<img src="https://res.pandateacher.com/492GV42L1594814047101.png" alt="img" style="zoom:50%;" />

## Summary

![img](https://res.pandateacher.com/OGF1OUR71604539640253.png)